[assembly: Xunit.CollectionBehavior(DisableTestParallelization = true)]

namespace Base2art.Data.EventSourcing.Samples.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.MessageQueue.Management;
    using Base2art.MessageQueue.Storage;
    using DataStorage;
    using EventSourcing.Features.Fixtures;
    using FluentAssertions;
    using MessageQueue;
    using Modeling;
    using Persistence.IsolatedObjectSystem;
    using Persistence.SharedObjectSystem;
    using Persistence.Tables;
    using Search;
    using Xunit;
    using Xunit.Abstractions;

    public class UnitTest2
    {
        private readonly ITestOutputHelper testOutputHelper;

        public UnitTest2(ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async void Test2()
        {
            await this.Test1(1);
            await this.Test1(1);
        }

        [Fact]
        public async void Test4()
        {
            await this.Test1(3);
            await this.Test1(3);
        }

        // bool isolate
//        [Fact]
        [Theory]
        [InlineData(1)]
//        [InlineData(2)]
        [InlineData(3)]
        public async Task Test1(int isolate)
        {
            MessageQueueSupport.ResetCache();
            this.testOutputHelper.WriteLine($"Start {isolate}");
//            bool isolate = true;
            var definer = new Base2art.DataStorage.InMemory.DataDefiner(true);
            var dbms = new Dbms(definer);

            var db = new DataStore(definer);

            IDataAccessReader<person> reader1 = null;
            IDataAccessWriter<person> writer1 = null;
            IDataAccessReader<company> reader2 = null;
            IDataAccessWriter<company> writer2 = null;
            IDataAccessReader<employment> reader3 = null;
            IDataAccessWriter<employment> writer3 = null;

            IDataAccessSearcher<employment_search_data, employment_published> reader4Simple = null;

            IDataAccessSearcher<composite_employment_search_data, composite_employment> reader4 = null;

            var queueStore = new InMemoryMessageQueueStore();
            var repairHandlerContainer = new RepairHandlerContainer();
            var queue = new MessageQueueRepair(queueStore, repairHandlerContainer);
            var messageHandlerService = new MessageHandlerService(queueStore, new[] {new RepairHandler(repairHandlerContainer)});

            if (isolate == 1)
            {
                var access1 = new GenericDataAccess<person>(null, db, queue);
                var repairer1 = new Repairer<person, person_published, es_shared_version_publication>(db, null);
                queue.AddRepairer<person>(repairer1);
                reader1 = access1;
                writer1 = access1;

                var access2 = new GenericDataAccess<company>(null, db, queue);
                var repairer2 = new Repairer<company, company_published, es_shared_version_publication>(db, null);
                queue.AddRepairer<company>(repairer2);

                reader2 = access2;
                writer2 = access2;

                var access3 = new GenericDataAccess<employment>(null, db, queue);
                var repairer3 = new Repairer<employment, employment_published, es_shared_version_publication>(db, null);
                queue.AddRepairer<employment>(repairer3);
                reader3 = access3;
                writer3 = access3;

                // new CustomQuery()
                var access4 =
                    new GenericSearchingManyManyDataAccess<composite_employment_search_data, employment_published, company_published, person_published
                    >(db, x => x.company_id, x => x.person_id, null);

                var access5 = new GenericSearchingDataAccess<employment_search_data, employment_published>(db, null);
                reader4 = new Wrapper(access4);
                reader4Simple = access5;
//                writer4 = access4;

                await access2.CreateTables(dbms);
                await access3.CreateTables(dbms);
                await repairer1.CreateTables(dbms);
                await repairer2.CreateTables(dbms);
                await repairer3.CreateTables(dbms);

                await access1.CreateTables(dbms);
                await access2.CreateTables(dbms);
                await access3.CreateTables(dbms);
                await repairer1.CreateTables(dbms);
                await repairer2.CreateTables(dbms);
                await repairer3.CreateTables(dbms);
            }
            else if (isolate == 2)
            {
//                var access = new PersonDataAccess(db, queue);
//                reader1 = access;
//                writer1 = access;
//                await access.CreateTables(dbms);
            }
            else if (isolate == 3)
            {
                var access1 = new PersonRepository(db, queue);
                reader1 = access1;
                writer1 = access1;
                await access1.CreateTables(dbms);

                var access2 = new CompanyRepository(db, queue);
                reader2 = access2;
                writer2 = access2;
                await access2.CreateTables(dbms);

                var access3 = new EmploymentRepository(db, queue);
                reader3 = access3;
                writer3 = access3;
                await access3.CreateTables(dbms);

                reader4 = access3;
                reader4Simple = access3;
            }

//            await Task.Delay(TimeSpan.FromSeconds(.02));
            var userId = Guid.NewGuid();
            var personId1 = await writer1.CreateAndPublishObject(new person_data {name = "Sjy", age = 21, is_citizen = true}, userId);
            var companyId1 = await writer2.CreateAndPublishObject(new company_data {name = "Base2art"}, userId);
            var employment1 = await writer3.CreateAndPublishObject(new employment_data
                                                                   {
                                                                       company_id = companyId1.ObjectId,
                                                                       person_id = personId1.ObjectId
                                                                   }, userId);

            var personId2 = await writer1.CreateAndPublishObject(new person_data {name = "MjY", age = 32, is_citizen = false}, userId);
            var companyId2 = await writer2.CreateAndPublishObject(new company_data {name = "B2A"}, userId);
            var employment2 = await writer3.CreateAndPublishObject(new employment_data
                                                                   {
                                                                       company_id = companyId2.ObjectId,
                                                                       person_id = personId2.ObjectId
                                                                   }, userId);

            await messageHandlerService.ProcessAll(this.testOutputHelper.WriteLine);

            var emp2 = await reader3.GetPublishedItem(employment2.ObjectId);
            emp2.Data.company_id.Should().NotBeEmpty();
            emp2.Data.company_id.Should().Be(companyId2.ObjectId);
            emp2.Data.person_id.Should().Be(personId2.ObjectId);

            var debugItems = await db
                                   .Select<employment_published>()
                                   .WithNoLock()
                                   .Execute();
//                                  .Join<TFirstAgg>(lambda1)
//                                  .Join<TSecondAgg>(lambda2)
//                                  .Where1(rs => this.Parameters1(rs, parameters))
//                                  .Where2(rs => this.Parameters2(rs, parameters))
//                                  .Where3(rs => this.Parameters3(rs, parameters))
////                                  .Join<TObjectVersionTable>((x, y, z, a) => x.version_id == a.version_id)
//                                  .Execute();

            debugItems.Count().Should().Be(2);
            var find = true;
            if (find)
            {
                var resultsAll1 = await reader4Simple.FindPublishedItems(new employment_search_data());
                resultsAll1.Count().Should().Be(2);

                var resultsAll = await reader4.FindPublishedItems(new composite_employment_search_data());
                resultsAll.Count().Should().Be(2);

                var results = await FindPublishedItemsString(reader4, StringOperation.Equal, "Sjy");
                results.Count().Should().Be(1);

                results = await FindPublishedItemsString(reader4, StringOperation.NotEqual, "MjY");
                results.Count().Should().Be(1);

                results = await FindPublishedItemsString(reader4, StringOperation.Like, "Sj%");
                results.Count().Should().Be(1);

                results = await FindPublishedItemsInt(reader4, CompOperation.Equal, 21);
                results.Count().Should().Be(1);

                results = await FindPublishedItemsInt(reader4, CompOperation.LessThan, 22);
                results.Count().Should().Be(1);

                results = await FindPublishedItemsInt(reader4, CompOperation.LessThan, 21);
                results.Count().Should().Be(0);

                results = await FindPublishedItemsInt(reader4, CompOperation.LessThanOrEqual, 22);
                results.Count().Should().Be(1);

                results = await FindPublishedItemsInt(reader4, CompOperation.GreaterThan, 20);
                results.Count().Should().Be(2);

                results = await FindPublishedItemsInt(reader4, CompOperation.GreaterThan, 21);
                results.Count().Should().Be(1);

                results = await FindPublishedItemsInt(reader4, CompOperation.GreaterThanOrEqual, 20);
                results.Count().Should().Be(2);

                results = await FindPublishedItemsBool(reader4, EqualityOperation.Equal, true);
                results.Count().Should().Be(1);
                results.First().Data.person.object_id.Should().NotBeEmpty();

                results = await FindPublishedItemsBool(reader4, EqualityOperation.Equal, null);
                results.Count().Should().Be(0);
                results = await FindPublishedItemsBool(reader4, EqualityOperation.NotEqual, null);
                results.Count().Should().Be(2);

                personId2 = await writer1.InsertAndPublishVersion(new person_data {name = "MjY", age = 32, is_citizen = null}, personId2, userId);

                results = await FindPublishedItemsBool(reader4, EqualityOperation.Equal, true);
                results.Count().Should().Be(1);

                await messageHandlerService.ProcessAll();

                results = await FindPublishedItemsBool(reader4, EqualityOperation.Equal, null);
                results.Count().Should().Be(1);
                results = await FindPublishedItemsBool(reader4, EqualityOperation.NotEqual, null);
                results.Count().Should().Be(1);

                results.First().Data.person.name.Should().Be("Sjy");

                await messageHandlerService.ProcessAll();
            }

            this.testOutputHelper.WriteLine($"End {isolate}");
        }

        private class Wrapper : IDataAccessSearcher<composite_employment_search_data, composite_employment>
        {
            private readonly GenericSearchingManyManyDataAccess<composite_employment_search_data, employment_published, company_published,
                    person_published>
                access4;

            public Wrapper(
                GenericSearchingManyManyDataAccess<composite_employment_search_data, employment_published, company_published, person_published>
                    access4)
            {
                this.access4 = access4;
            }

            public async Task<IEnumerable<IObjectEntityRow<composite_employment>>> FindPublishedItems(composite_employment_search_data parameters)
            {
                var items = await this.access4.FindPublishedItems(parameters);
                return items.Select(x => CompositeObjectEntityRowMapper3
                                        .MapTo<composite_employment, composite_employment_data, employment_published, company_published,
                                            person_published
                                        >(x, "employment", this.access4.FirstId(), this.access4.SecondId()));
            }

            public async Task<IEnumerable<IObjectEntityRow<composite_employment>>> FindPublishedItems(composite_employment_search_data parameters, IndexPagination pagination)
            {
                var items = await this.access4.FindPublishedItems(parameters, pagination);
                return items.Select(x => CompositeObjectEntityRowMapper3
                                        .MapTo<composite_employment, composite_employment_data, employment_published, company_published,
                                            person_published
                                        >(x, "employment", this.access4.FirstId(), this.access4.SecondId()));
            }

            public async Task<IEnumerable<IObjectEntityRow<composite_employment>>> FindPublishedItems(composite_employment_search_data parameters, OffsetPagination pagination)
            {
                var items = await this.access4.FindPublishedItems(parameters, pagination);
                return items.Select(x => CompositeObjectEntityRowMapper3
                                        .MapTo<composite_employment, composite_employment_data, employment_published, company_published,
                                            person_published
                                        >(x, "employment", this.access4.FirstId(), this.access4.SecondId()));
            }
        }

        private static Task<IEnumerable<IObjectEntityRow<composite_employment>>>
            FindPublishedItemsString(
                IDataAccessSearcher<composite_employment_search_data, composite_employment> reader4,
                StringOperation op,
                string value) =>
            reader4.FindPublishedItems(new composite_employment_search_data
                                       {
                                           person = new person_search_data()
                                                    {
                                                        name = new StringSearchParameters
                                                               {
                                                                   Operation = op,
                                                                   Value = value
                                                               }
                                                    }
                                       });

        private static Task<IEnumerable<IObjectEntityRow<composite_employment>>>
            FindPublishedItemsInt(
                IDataAccessSearcher<composite_employment_search_data, composite_employment> reader4,
                CompOperation op,
                int value) =>
            reader4.FindPublishedItems(new composite_employment_search_data
                                       {
                                           person = new person_search_data
                                                    {
                                                        age = new Int32SearchParameters
                                                              {
                                                                  Operation = op,
                                                                  Value = value
                                                              }
                                                    }
                                       });

        private static Task<IEnumerable<IObjectEntityRow<composite_employment>>>
            FindPublishedItemsBool(
                IDataAccessSearcher<composite_employment_search_data, composite_employment> reader4,
                EqualityOperation op,
                bool? value) =>
            reader4.FindPublishedItems(new composite_employment_search_data
                                       {
                                           person = new person_search_data
                                                    {
                                                        is_citizen = new BooleanSearchParameters
                                                                     {
                                                                         Operation = op,
                                                                         Value = value
                                                                     }
                                                    }
                                       });

//        public class CustomQuery : IManyManyQueryFilter<composite_employment_search_data, employment_published, company_published, person_published>
//        {
//            public void Where1(IWhereClauseBuilder<employment_published> rs, composite_employment_search_data parameters)
//            {
//                var searcher = new ConventionalQueryFilter<employment_search_data, employment_published>();
//                searcher.Filter(rs, parameters.employment);
//            }
//
//            public void Where3(IWhereClauseBuilder<person_published> rs, composite_employment_search_data parameters)
//            {
//                var searcher = new ConventionalQueryFilter<person_search_data, person_published>();
//                searcher.Filter(rs, parameters.person);
//            }
//
//            public void Where2(IWhereClauseBuilder<company_published> rs, composite_employment_search_data parameters)
//            {
//                var searcher = new ConventionalQueryFilter<company_search_data, company_published>();
//                searcher.Filter(rs, parameters.company);
//            }
//        }

        internal class composite_employment_data : composite_employment
        {
            public company_published company { get; set; }

            public person_published person { get; set; }

            public employment employment { get; set; }
        }
    }
}