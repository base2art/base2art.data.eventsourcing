namespace Base2art.Data.EventSourcing.Samples.Features
{
    using System;
    using System.Linq;
    using Base2art.MessageQueue.Management;
    using Base2art.MessageQueue.Storage;
    using DataStorage;
    using EventSourcing.Features;
    using EventSourcing.Features.Fixtures;
    using FluentAssertions;
    using MessageQueue;
    using Search;
    using Xunit;

    public class MentorshipFeatures
    {
        [Fact]
        public async void Test2()
        {
            MessageQueueSupport.ResetCache();

            var definer = new Base2art.DataStorage.InMemory.DataDefiner(true);
            var dbms = new Dbms(definer);

            var db = new DataStore(definer);

            var queueStore = new InMemoryMessageQueueStore();
            var repairHandlerContainer = new RepairHandlerContainer();
            var queue = new MessageQueueRepair(queueStore, repairHandlerContainer);
            var messageHandlerService = new MessageHandlerService(queueStore, new[] {new RepairHandler(repairHandlerContainer)});

            var people = new PersonRepository(db, queue);
            await people.CreateTables(dbms);
            var mentors = new MentorshipRepository(db, queue);
            await mentors.CreateTables(dbms);

            var userId = Guid.NewGuid();
            var personId1 = await people.CreateAndPublishObject(new person_data {name = "Sjy", age = 21, is_citizen = true}, userId);
            var personId2 = await people.CreateAndPublishObject(new person_data {name = "MjY", age = 31, is_citizen = true}, userId);

            var personId3 = await people.CreateAndPublishObject(new person_data {name = "CP", age = 21, is_citizen = true}, userId);
            var personId4 = await people.CreateAndPublishObject(new person_data {name = "TS", age = 31, is_citizen = true}, userId);

            await messageHandlerService.ProcessAll();

            var relationship1 = await mentors.CreateAndPublishObject(new mentorship_data {mentee = personId1.ObjectId, mentor_id = personId2.ObjectId}, userId);
            var relationship2 = await mentors.CreateAndPublishObject(new mentorship_data {mentee = personId3.ObjectId, mentor_id = personId4.ObjectId}, userId);

            await messageHandlerService.ProcessAll();

            var items = await mentors.FindPublishedItems(new composite_mentorship_search_data
                                                         {
                                                             mentor = new person_search_data
                                                                      {
                                                                          name = new StringSearchParameters
                                                                                 {
                                                                                     Operation = StringOperation.Equal,
                                                                                     Value = "MjY"
                                                                                 }
                                                                      }
                                                         });

            items.Count().Should().Be(1);
            items.First().Data.mentor.name.Should().Be("MjY");
            items.First().Data.mentee.name.Should().Be("Sjy");
        }
    }
}