namespace Base2art.Data.EventSourcing.Samples.Features
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.MessageQueue.Management;
    using Base2art.MessageQueue.Storage;
    using DataStorage;
    using EventSourcing.Features.Fixtures;
    using FluentAssertions;
    using MessageQueue;
    using Search;
    using Xunit;

    public class UnitTest3
    {
        [Fact]
        public async void Test2()
        {
            await this.Test1(1);
            await this.Test1(2);
        }

        public async Task Test1(int number)
        {
            MessageQueueSupport.ResetCache();

            var definer = new Base2art.DataStorage.InMemory.DataDefiner(true);
            var dbms = new Dbms(definer);

            var db = new DataStore(definer);

            
            var queueStore = new InMemoryMessageQueueStore();
            var repairHandlerContainer = new RepairHandlerContainer();
            var queue = new MessageQueueRepair(queueStore, repairHandlerContainer);
            var messageHandlerService = new MessageHandlerService(queueStore, new[] {new RepairHandler(repairHandlerContainer)});

            var access1 = new PersonRepository(db, queue);
            await access1.CreateTables(dbms);

            var userId = Guid.NewGuid();
            var personId1 = await access1.CreateAndPublishObject(new person_data {name = "Sjy", age = 21, is_citizen = true}, userId);

            await messageHandlerService.ProcessAll();

            var emp2 = await access1.GetPublishedItem(personId1.ObjectId);
            emp2.Data.name.Should().NotBeEmpty();
            emp2.Data.name.Should().Be("Sjy");
            var results = await access1.FindPublishedItems(new person_search_data
                                                           {
                                                               name = new StringSearchParameters
                                                                      {
                                                                          Value = "Sj%",
                                                                          Operation = StringOperation.Like
                                                                      }
                                                           });
            results.Count().Should().Be(1);
        }
    }
}