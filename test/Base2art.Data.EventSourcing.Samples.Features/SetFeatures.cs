namespace Base2art.Data.EventSourcing.Samples.Features
{
    using System.Linq;
    using EventSourcing.Features.Fixtures;
    using FluentAssertions;
    using Utilities.Diffing;
    using Xunit;

    public class SetFeatures
    {
        [Fact]
        public void ShouldGetDifferencesSymmetric()
        {
            var @old = new TestPerson {Labels = new string[] {"1", "2"}};
            var @new = new TestPerson {Labels = new string[] {"3", "2"}};

            var differ = Differ.CreateSymmetric(old, @new);
            var result = differ.Compare(x => x.Labels);

            result.Adds.Should().HaveCount(1).And.Subject.First().Should().Be("3");
            result.Removes.Should().HaveCount(1).And.Subject.First().Should().Be("1");
        }

        [Fact]
        public void ShouldGetDifferencesAsymmetric()
        {
            var @old1 = new PersonLabel {label = "3", id = 3};
            var @old2 = new PersonLabel {label = "2", id = 2};

            var @new = new TestPerson {Labels = new string[] {"1", "2"}};
//            var @new = new TestPerson {Labels = new string[] {"3", "2"}};

            var differ = Differ.CreateAsymmetric(new[] {old1, old2}, @new);
            var result = differ.Compare(x => x.label, x => x.Labels);

            result.Adds.Should().HaveCount(1).And.Subject.First().Should().Be("1");
            result.Removes.Should().HaveCount(1).And.Subject.First().id.Should().Be(3);
        }
    }

    public class PersonLabel
    {
        public string label;

        public int id;
//        public string[] labels;
    }
}