namespace Base2art.Data.EventSourcing.Samples.Features
{
    using System;
    using Base2art.MessageQueue.Management;
    using Base2art.MessageQueue.Storage;
    using DataStorage;
    using DataStorage.DataManipulation.Builders;
    using EventSourcing.Features.Fixtures;
    using FluentAssertions;
    using MessageQueue;
    using Persistence;
    using Persistence.SharedObjectSystem;
    using Xunit;

    public class UnitTest1
    {
        [Theory]
        [InlineData(1, true)]
//        [InlineData(2, true)]
        [InlineData(1, false)]
//        [InlineData(2, false)]
        [InlineData(3, true)]
        [InlineData(3, false)]
        public async void Test1(int isolate, bool customBinder)
        {
            MessageQueueSupport.ResetCache();
            var definer = new Base2art.DataStorage.InMemory.DataDefiner(true);
            var dbms = new Dbms(definer);

            var db = new DataStore(definer);

            IDataAccessReader<person> reader = null;
            IDataAccessWriter<person> writer = null;

            var queueStore = new InMemoryMessageQueueStore();
            var repairHandlerContainer = new RepairHandlerContainer();
            var queue = new MessageQueueRepair(queueStore, repairHandlerContainer);
            var messageHandlerService = new MessageHandlerService(queueStore, new[] {new RepairHandler(repairHandlerContainer)});

            if (isolate == 1)
            {
                var access = new GenericDataAccess<person>(customBinder ? new PersonDataBinder() : null, db, queue);
                reader = access;
                writer = access;
                await access.CreateTables(dbms);
            }
            else if (isolate == 2)
            {
//                var access = new PersonDataAccess(db, queue);
//                reader = access;
//                writer = access;
//                await access.CreateTables(dbms);
            }
            else if (isolate == 3)
            {
                var access = new PersonRepository(db, queue);
                reader = access;
                writer = access;
                await access.CreateTables(dbms);
            }

            var id1 = await writer.CreateObject(new person_data {name = "Sjy"}, Guid.NewGuid());

            var id = id1.ObjectId;
            var obj = await reader.GetLatestItem(id);

            obj.Data.name.Should().Be("Sjy");
            var pub = await reader.GetPublishedItem(id);
            pub.Should().BeNull();
            await writer.PublishVersion(obj.Id, obj.RepresentedVersion.VersionId, Guid.NewGuid());

            pub = await reader.GetPublishedItem(id);
            pub.Data.name.Should().Be("Sjy");

            await writer.UnpublishObject(id, Guid.NewGuid());
            pub = await reader.GetPublishedItem(id);
            pub.Should().BeNull();
        }

        private class PersonDataBinder : IDataBinder<person>
        {
            public ISetListBuilder<person> Bind(ISetListBuilder<person> rs, person data)
            {
                return rs.Field(x => x.name, data.name);
            }
        }
    }
}