//namespace Base2art.Data.EventSourcing.Features
//{
//    using System;
//    using System.Threading.Tasks;
//    using DataStorage;
//    using Fixtures;
//    using Modeling;
//    using Persistence.IsolatedObjectSystem;
//    using Persistence.Tables;
//
//    public class PersonDataAccess : IDataAccessWriter<person>, IDataAccessReader<person>
//    {
//        private readonly GenericIsolatedDataAccess<person, person_object, person_version, person_publication> backing;
//        private readonly Repairer<person, person_published, person_publication> repairer1;
//
//        public PersonDataAccess(IDataStore store, IDataAccessRepairQueue queue)
//        {
//            this.repairer1 = new Repairer<person, person_published, person_publication>(store, null);
//            queue.AddRepairer<person>(this.repairer1);
//            this.backing = new GenericIsolatedDataAccess<person, person_object, person_version, person_publication>(
//                                                                                                                    null,
//                                                                                                                    store,
//                                                                                                                    queue);
//        }
//
//        public Task<ObjectVersionId> CreateObject(person data, Guid userId)
//            => this.backing.CreateObject(data, userId);
//
//        public Task<Guid> InsertVersion(Guid id, person data, Guid previousVersion, Guid userId)
//            => this.backing.InsertVersion(id, data, previousVersion, userId);
//
//        public Task UnpublishObject(Guid id, Guid userId)
//            => this.backing.UnpublishObject(id, userId);
//
//        public Task PublishVersion(Guid id, Guid version, Guid userId)
//            => this.backing.PublishVersion(id, version, userId);
//
//        public Task<IObjectEntity<person>> GetPublishedItem(Guid id)
//            => this.backing.GetPublishedItem(id);
//
//        public Task<IObjectEntity<person>> GetLatestItem(Guid id)
//            => this.backing.GetLatestItem(id);
//
//        public Task<IObjectEntity<person>> GetItem(Guid id, Guid versionId)
//            => this.backing.GetItem(id, versionId);
//
//        public async Task CreateTables(IDbms dbms)
//        {
//            await this.repairer1.CreateTables(dbms);
//            await this.backing.CreateTables(dbms);
//        }
//
//        private interface person_object : es_object
//        {
//        }
//
//        private interface person_version : es_version
//        {
//        }
//
//        private interface person_publication : es_version_publication
//        {
//        }
//
//        private interface person_published : person, es_agg_published
//        {
//        }
//    }
//}