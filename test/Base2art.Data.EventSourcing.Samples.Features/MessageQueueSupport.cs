namespace Base2art.Data.EventSourcing.Samples.Features
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    using Base2art.MessageQueue.Management;

    public static class MessageQueueSupport
    {
        public static void ResetCache()
        {
            var asm = typeof(Base2art.MessageQueue.Management.MessageHandlerService).Assembly;
            var type = asm.GetType("Base2art.MessageQueue.Management.Internals.MessageHandlers");
            var field = type.GetField("typeMapping", BindingFlags.Static | BindingFlags.NonPublic);

            var obj = (System.Collections.IDictionary) field.GetValue(null);
            obj.Clear();
        }

        public static async Task ProcessAll(this MessageHandlerService queue, Action<string> writeLine = null)
        {
            var oldOut = Console.Out;
            using (var stringWriter = new StringWriter())
            {
                Console.SetOut(stringWriter);

                for (int i = 0; i < 100; i++)
                {
                    await queue.ProcessItem();
                }

                writeLine?.Invoke(stringWriter.GetStringBuilder().ToString());
                Console.SetOut(oldOut);
            }
        }
    }
}