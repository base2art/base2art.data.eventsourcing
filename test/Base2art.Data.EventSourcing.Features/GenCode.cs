namespace Base2art.Data.EventSourcing.Features
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    using CodeGen;
    using FluentAssertions;
    using Xunit;
    using Xunit.Abstractions;

    public class GenCode
    {
        private readonly ITestOutputHelper helper;

        public GenCode(ITestOutputHelper helper)
        {
            this.helper = helper;
        }

        [Fact]
        public async Task ShouldLoad()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;

            string localPath = new Uri(codeBase).LocalPath;
            var rootDir = System.IO.Path.GetDirectoryName(localPath);

            var dir = new DirectoryInfo(rootDir);

            while (!string.Equals("Base2art.Data.EventSourcing.Features", dir.Name))
            {
                dir = dir.Parent;
            }

            var root = dir.Parent.Parent;
            this.helper.WriteLine(root.FullName);

            {
                var scanDir = Path.Combine(root.FullName, "fixtures", "Base2art.Data.EventSourcing.Samples", "Tables");
                var outDir = Path.Combine(root.FullName, "fixtures", "Base2art.Data.EventSourcing.Samples", "gen");


                var gen = new CodeGen(Directory.CreateDirectory(scanDir), Directory.CreateDirectory(outDir));
                await gen.Process(false);
            }
            {
                var scanDir = Path.Combine(root.FullName, "fixtures", "Base2art.Data.EventSourcing.Ipadb", "Tables");
                var outDir = Path.Combine(root.FullName, "fixtures", "Base2art.Data.EventSourcing.Ipadb", "gen");


                var gen = new CodeGen(Directory.CreateDirectory(scanDir), Directory.CreateDirectory(outDir));
                await gen.Process(false);
            }
        }

        [Fact]
        public async Task ShouldCamelCase()
        {
            "person".ToCamelCase().Should().Be("Person");
            "person_role".ToCamelCase().Should().Be("PersonRole");
        }
    }
}