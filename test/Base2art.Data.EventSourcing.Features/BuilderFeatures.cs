//namespace Base2art.Data.EventSourcing.Features
//{
//    using CodeGen;
//    using CodeGen.RiotGen;
//    using FluentAssertions;
//    using Xunit;
//
//    public class BuilderFeatures
//    {
//        [Fact]
//        public async void ShouldBuildSimple()
//        {
//            var builder = new RiotHtmlBuilder("test");
//            builder.ToString().NoLines().Should().Be($"<test></test>");
//        }
//        
//        [Fact]
//        public async void ShouldBuildSimpleWithChildren()
//        {
//            var builder = new RiotHtmlBuilder("test");
//            using (var @virtual = builder.Node("virtual"))
//            {
//                @virtual.WithIf("opts.model.title");
//                using (@virtual.Node("H3"))
//                {
//                    
//                }
//                @virtual.Binding("opts.model");
//                using (@virtual.Node("h4"))
//                {
//                    
//                }
//            }
//            
//
//            builder.ToString().Should().Be($@"
//<test>
//  <virtual if={{opts.model.title}}>
//    <H3></H3>
//    {{opts.model}}
//    <h4></h4>
//  </virtual>
//</test>".Trim());
//        }
//    }
//}