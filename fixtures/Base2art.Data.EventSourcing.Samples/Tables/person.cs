// ReSharper disable InconsistentNaming

namespace Base2art.Data.EventSourcing.Features.Fixtures
{
    using System;
    using Persistence;

    public interface person : ITableRow
    {
        string name { get; }
        int age { get; }
        bool? is_citizen { get; }
    }
}