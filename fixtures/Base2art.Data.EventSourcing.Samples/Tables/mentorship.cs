// ReSharper disable InconsistentNaming

namespace Base2art.Data.EventSourcing.Features.Fixtures
{
    using System;
    using Persistence;

    public interface mentorship : ITableRow
    {
        [RelatesTo(nameof(person))]
        Guid mentor_id { get; }

        [RelatesTo("person")]
        Guid mentee { get; }
    }
}