// ReSharper disable InconsistentNaming
namespace Base2art.Data.EventSourcing.Features.Fixtures
{
    using System;
    using Persistence;

    public interface company : ITableRow
    {
        string name { get; }
    }
}