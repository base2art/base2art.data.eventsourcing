// ReSharper disable InconsistentNaming

namespace Base2art.Data.EventSourcing.Features.Fixtures
{
    using System;
    using Persistence;

    public interface employment : ITableRow
    {
        [RelatesTo(nameof(company))]
        Guid company_id { get; }

        [RelatesTo("person")]
        Guid person_id { get; }
    }
}