namespace Base2art.Data.EventSourcing.Ipadb
{
    using System;
    using Base2art.Data.EventSourcing;
    using Base2art.Data.EventSourcing.Ipadb;
    using Base2art.Data.EventSourcing.Persistence;

    public interface lineup_performers : ITableRow
    {
        [RelatesTo(nameof(lineup))]
        Guid lineup_id { get; }
        
        [RelatesTo(nameof(performer))]
        Guid performer_id { get; }
    }
}