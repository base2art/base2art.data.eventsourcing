namespace Base2art.Data.EventSourcing.Ipadb
{
    using System;
    using Base2art.Data.EventSourcing;
    using Base2art.Data.EventSourcing.Ipadb;
    using Base2art.Data.EventSourcing.Persistence;

    public interface performance : ITableRow
    {
        DateTimeOffset when { get; }

        [RelatesTo(nameof(location))]
        Guid location_id { get; set; }

        [RelatesTo(nameof(lineup))]
        Guid lineup_id { get; set; }

        [RelatesTo(nameof(show))]
        Guid show_id { get; set; }

        [RelatesTo(nameof(troupe))]
        Guid troupe_id { get; set; }
    }
}