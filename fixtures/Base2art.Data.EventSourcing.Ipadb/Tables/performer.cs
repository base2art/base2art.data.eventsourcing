namespace Base2art.Data.EventSourcing.Ipadb
{
    using System;
    using Base2art.Data.EventSourcing.Persistence;

    public interface performer : ITableRow
    {
//        Guid id { get; }
        string display_id { get; }

        string name { get; }

        string biography { get; }
        string images { get; }
    }
}