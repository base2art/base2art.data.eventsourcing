namespace Base2art.Data.EventSourcing.Ipadb
{
    using System;
    using Base2art.Data.EventSourcing;
    using Base2art.Data.EventSourcing.Persistence;

    public interface show : ITableRow
    {
        [RelatesTo(nameof(troupe))]
        Guid troupe_id { get; }

        string display_id { get; }
        string name { get; set; }
        string description { get; set; }
    }
}