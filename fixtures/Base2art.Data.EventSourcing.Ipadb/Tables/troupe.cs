namespace Base2art.Data.EventSourcing.Ipadb
{
    using System;
    using Base2art.Data.EventSourcing;
    using Base2art.Data.EventSourcing.Ipadb;
    using Base2art.Data.EventSourcing.Persistence;

    public interface troupe : ITableRow
    {
        [RelatesTo(nameof(lineup))]
        Guid default_lineup_id { get; set; }
    }
}