namespace Base2art.Data.EventSourcing.CodeGen
{
    using System.Linq;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using Persistence.Tables;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class RepositoryInterfaceFileGenerator : FileGeneratorBase, IGenerator
    {
        public override GeneratorDataType DataType() => GeneratorDataType.Repository;

        public override string TypeName(string classDeclaration, PropertyId[] properties) => $"I{classDeclaration.ToCamelCase()}Repository";

        protected override TypeDeclarationSyntax TypeDeclaration(string className)
            => SyntaxFactory.InterfaceDeclaration(className);

        protected override BaseListSyntax BaseTypes(string classDeclaration, PropertyId[] properties)
        {
            var searcher = $"Base2art.Data.EventSourcing.IDataAccessSearcher<{classDeclaration}_search_data, {classDeclaration}_published>";
            var writer = $"Base2art.Data.EventSourcing.IDataAccessWriter<{classDeclaration}>";
            var reader = $"Base2art.Data.EventSourcing.IDataAccessReader<{classDeclaration}>";

            var relatesTo = properties.Select(x => x.RelatesTo).Where(x => x != null).ToArray();
            var relatesToCount = relatesTo.Length;
//            var manyManySearcher = $"Base2art.Data.EventSourcing.I{"Many".Repeat(relatesToCount)}DataAccessSearcher<"
//                                   + $"composite_{classDeclaration}_search_data, "
//                                   + $"{classDeclaration}_published, "
//                                   + $"{string.Join(", ", relatesTo.Select(y => $"{y}_published"))}>";

            var manyManySearcher = $"Base2art.Data.EventSourcing.IDataAccessSearcher<"
                                   + $"composite_{classDeclaration}_search_data, "
                                   + $"composite_{classDeclaration}>";

            if (relatesToCount >= 2)
            {
                return BaseList(SeparatedList<BaseTypeSyntax>(
                                                              new SyntaxNodeOrToken[]
                                                              {
                                                                  SimpleBaseType(IdentifierName(reader)),
                                                                  Token(SyntaxKind.CommaToken).WithTrailingTrivia(LineFeed),

                                                                  SimpleBaseType(IdentifierName(writer)),
                                                                  Token(SyntaxKind.CommaToken).WithTrailingTrivia(LineFeed),
                                                                  SimpleBaseType(IdentifierName(searcher)),
                                                                  Token(SyntaxKind.CommaToken).WithTrailingTrivia(LineFeed),
                                                                  SimpleBaseType(IdentifierName(manyManySearcher)),
                                                              }));
            }

            return BaseList(SeparatedList<BaseTypeSyntax>(
                                                          new SyntaxNodeOrToken[]
                                                          {
                                                              SimpleBaseType(IdentifierName(reader)),
                                                              Token(SyntaxKind.CommaToken).WithTrailingTrivia(LineFeed),
                                                              SimpleBaseType(IdentifierName(writer)),
                                                              Token(SyntaxKind.CommaToken).WithTrailingTrivia(LineFeed),
                                                              SimpleBaseType(IdentifierName(searcher)),
                                                          }));
        }

        protected override SyntaxList<UsingDirectiveSyntax> Usings()
            => List(
                    new[]
                    {
                        UsingDirective(IdentifierName("System")),
                        UsingDirective(IdentifierName("Base2art.Data.EventSourcing.Persistence")),
                        UsingDirective(IdentifierName("Base2art.Data.EventSourcing.Persistence.Tables"))
                    });
    }
}