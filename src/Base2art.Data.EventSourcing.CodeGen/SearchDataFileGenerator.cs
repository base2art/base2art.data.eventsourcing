namespace Base2art.Data.EventSourcing.CodeGen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection.Metadata;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using Persistence.Tables;
    using Search;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class SearchDataFileGenerator : FileGeneratorBase
    {
        public override GeneratorDataType DataType() => GeneratorDataType.Search;

        public override string TypeName(string classDeclaration, PropertyId[] properties) => $"{classDeclaration}_search_data";

        protected override TypeDeclarationSyntax TypeDeclaration(string className)
            => ClassDeclaration(className);

        protected override SyntaxList<MemberDeclarationSyntax> Members(string classDeclaration, PropertyId[] props)
        {
            return List(props.Select(this.Map)
                             .Where(x => x != null)
                             .ToArray());
        }

        private MemberDeclarationSyntax Map(PropertyId propertyId)
        {
            var mapType = this.MapType(propertyId.Type);
            return string.IsNullOrWhiteSpace(mapType)
                       ? null
                       : this.GetSetProperty(propertyId.Name, mapType);
        }

        private string MapType(string propertyIdType)
        {
            propertyIdType = propertyIdType.Reverse()
                                           .SkipWhile(x => x == '?')
                                           .Reverse()
                                           .AsString();

            var map = new Dictionary<string, string>();
            map[Token(SyntaxKind.BoolKeyword).Text] = nameof(BooleanSearchParameters);
            map[nameof(Boolean)] = nameof(BooleanSearchParameters);

            map[Token(SyntaxKind.ByteKeyword).Text] = nameof(ByteSearchParameters);
            map[nameof(Byte)] = nameof(ByteSearchParameters);

            map[nameof(DateTime)] = nameof(DateTimeSearchParameters);

            map[nameof(DateTimeOffset)] = nameof(DateTimeOffsetSearchParameters);

            map[Token(SyntaxKind.DecimalKeyword).Text] = nameof(DecimalSearchParameters);
            map[nameof(Decimal)] = nameof(DecimalSearchParameters);

            map[Token(SyntaxKind.DoubleKeyword).Text] = nameof(DoubleSearchParameters);
            map[nameof(Double)] = nameof(DoubleSearchParameters);

            map[nameof(Guid)] = nameof(GuidSearchParameters);

            map[Token(SyntaxKind.ShortKeyword).Text] = nameof(Int16SearchParameters);
            map[nameof(Int16)] = nameof(Int16SearchParameters);

            map[Token(SyntaxKind.IntKeyword).Text] = nameof(Int32SearchParameters);
            map[nameof(Int32)] = nameof(Int32SearchParameters);

            map[Token(SyntaxKind.LongKeyword).Text] = nameof(Int64SearchParameters);
            map[nameof(Int64)] = nameof(Int64SearchParameters);

            map[Token(SyntaxKind.FloatKeyword).Text] = nameof(SingleSearchParameters);
            map[nameof(Single)] = nameof(SingleSearchParameters);

            map[Token(SyntaxKind.StringKeyword).Text] = nameof(StringSearchParameters);
            map[nameof(String)] = nameof(StringSearchParameters);

            map[nameof(TimeSpan)] = nameof(TimeSpanSearchParameters);

            return map.ContainsKey(propertyIdType) ? map[propertyIdType] : null;
        }

        protected override SyntaxList<UsingDirectiveSyntax> Usings() =>
            SyntaxFactory.List(
                               new[]
                               {
                                   SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("System")),
                                   SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("Base2art.Data.EventSourcing.Persistence")),
                                   SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("Base2art.Data.EventSourcing.Persistence.Tables")),
                                   SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("Base2art.Data.EventSourcing.Search"))
                               });
    }
}