namespace Base2art.Data.EventSourcing.CodeGen
{
    public class PropertyId
    {
        public string Type { get; set; }
        public bool HasSet { get; set; }
        public bool HasGet { get; set; }
        public string Name { get; set; }
        public string RelatesTo { get; set; }
    }
}