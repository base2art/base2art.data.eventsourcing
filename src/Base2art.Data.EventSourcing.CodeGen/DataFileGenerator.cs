namespace Base2art.Data.EventSourcing.CodeGen
{
    using System.Linq;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class DataFileGenerator : FileGeneratorBase, IGenerator
    {
        public override GeneratorDataType DataType() => GeneratorDataType.Table;

        public override string TypeName(string classDeclaration, PropertyId[] properties) => $"{classDeclaration}_data";

        protected override TypeDeclarationSyntax TypeDeclaration(string className) =>
            ClassDeclaration(className);

        protected override BaseListSyntax BaseTypes(string classDeclaration, PropertyId[] properties)
            => BaseList(SingletonSeparatedList<BaseTypeSyntax>(SimpleBaseType(IdentifierName(classDeclaration))));

        protected override SyntaxList<MemberDeclarationSyntax> Members(string classDeclaration, PropertyId[] props)
        {
            return List(
                        new[] {this.GetSetProperty("id", "Guid")}
                            .Concat(props.Select(x => this.GetSetProperty(x.Name, x.Type))));
        }

        protected override SyntaxList<UsingDirectiveSyntax> Usings() =>
            List(
                 new[]
                 {
                     UsingDirective(IdentifierName("System")),
                     UsingDirective(IdentifierName("Base2art.Data.EventSourcing.Persistence"))
                 });
    }
}