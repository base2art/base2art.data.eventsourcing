namespace Base2art.Data.EventSourcing.CodeGen
{
    public enum GeneratorDataType
    {
        Table,
        Search,
        Unknown,
        Data,
        Repository,
        Repository_Impl,
        Json,
        RiotTemplate
    }
}