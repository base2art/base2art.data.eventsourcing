namespace Base2art.Data.EventSourcing.CodeGen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using Modeling;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
    using Parameter = System.Reflection.Metadata.Parameter;

    public class RepositoryImplementationFileGenerator : FileGeneratorBase, IGenerator
    {
        public override GeneratorDataType DataType() => GeneratorDataType.Repository_Impl;

        public override string TypeName(string classDeclaration, PropertyId[] properties) => $"{classDeclaration.ToCamelCase()}Repository";

        protected override TypeDeclarationSyntax TypeDeclaration(string className)
            => ClassDeclaration(className);

        protected override SyntaxList<MemberDeclarationSyntax> Members(string classDeclaration, PropertyId[] props)
        {
            var relatesToPairs = props.Select(x => (Name: x.Name, RelatesTo: x.RelatesTo)).Where(x => x.RelatesTo != null).ToArray();
            var relatesTo = relatesToPairs.Select(x => x.RelatesTo).Where(x => x != null).ToArray();
            var relatesToCount = relatesTo.Length;

            var items = new List<MemberDeclarationSyntax>
                        {
                            this.Method(
                                        "backingReader",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "IObjectEntity", classDeclaration),
                                        "GetPublishedItem",
                                        Tuple.Create("Guid", "id")),

                            this.Method(
                                        "backingReader",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "IObjectEntity", classDeclaration),
                                        "GetLatestItem",
                                        Tuple.Create("Guid", "id")),

                            this.Method(
                                        "backingReader",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "IObjectEntity", classDeclaration),
                                        "GetItem",
                                        Tuple.Create("Guid", "id"),
                                        Tuple.Create("Guid", "versionId")),

                            this.Method(
                                        "backingReader",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "ObjectVersionId"),
                                        "CreateObject",
                                        Tuple.Create(classDeclaration, "data"),
                                        Tuple.Create("Guid", "userId")),

                            this.Method(
                                        "backingReader",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "Guid"),
                                        "InsertVersion",
                                        Tuple.Create(nameof(Guid), "id"),
                                        Tuple.Create(classDeclaration, "data"),
                                        Tuple.Create(nameof(Guid), "previousVersion"),
                                        Tuple.Create("Guid", "userId")),

                            this.Method(
                                        "backingReader",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task)),
                                        "UnpublishObject",
                                        Tuple.Create(nameof(Guid), "id"),
                                        Tuple.Create("Guid", "userId")),
                            this.Method(
                                        "backingReader",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task)),
                                        "PublishVersion",
                                        Tuple.Create(nameof(Guid), "id"),
                                        Tuple.Create(nameof(Guid), "versionId"),
                                        Tuple.Create("Guid", "userId")),

                            this.Method(
                                        "backingSearcher1",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "IEnumerable", "IObjectEntityRow", $"{classDeclaration}_published"),
                                        "FindPublishedItems",
                                        Tuple.Create($"{classDeclaration}_search_data", "parameters")),

                            this.Method(
                                        "backingSearcher1",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "IEnumerable", "IObjectEntityRow", $"{classDeclaration}_published"),
                                        "FindPublishedItems",
                                        Tuple.Create($"{classDeclaration}_search_data", "parameters"),
                                        Tuple.Create($"Base2art.Data.EventSourcing.Modeling.IndexPagination", "pagination")),

                            this.Method(
                                        "backingSearcher1",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "IEnumerable", "IObjectEntityRow", $"{classDeclaration}_published"),
                                        "FindPublishedItems",
                                        Tuple.Create($"{classDeclaration}_search_data", "parameters"),
                                        Tuple.Create($"Base2art.Data.EventSourcing.Modeling.OffsetPagination", "pagination")),

                            this.Method(
                                        new[] {"backingReader", "repairer1"},
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task)),
                                        "CreateTables",
                                        Tuple.Create($"IDbms", "dbms")),
                        };

            if (relatesToCount >= 1)
            {
//                items.Add(this.Method(
//                                      "backingSearcher1_1",
//                                      SyntaxKind.PublicKeyword,
//                                      this.TypeOf(
//                                                  new string[] {nameof(Task), "IEnumerable", "IObjectEntityRow"},
//                                                  this.FatTypeOf("Tuple", new[] {$"{classDeclaration}_published"}
//                                                                          .Concat(relatesTo.Select(y => $"{y}_published")).ToArray())),
//                                      "FindPublishedItems",
//                                      Tuple.Create($"composite_{classDeclaration}_search_data", "parameters")));

                var listOfCalls = new List<StatementSyntax>();
                var callOfBacking = AwaitExpression(
                                                    InvocationExpression(
                                                                         MemberAccessExpression(
                                                                                                SyntaxKind.SimpleMemberAccessExpression,
                                                                                                MemberAccessExpression(
                                                                                                                       SyntaxKind
                                                                                                                           .SimpleMemberAccessExpression,
                                                                                                                       ThisExpression(),
                                                                                                                       IdentifierName("backingSearcher1_1")),
                                                                                                IdentifierName("FindPublishedItems")))
                                                        .WithArgumentList(
                                                                          ArgumentList(
                                                                                       SingletonSeparatedList(
                                                                                                              Argument(
                                                                                                                       IdentifierName("parameters"))))));
                listOfCalls.Add(LocalDeclarationStatement(
                                                          VariableDeclaration(IdentifierName("var"))
                                                              .WithVariables(
                                                                             SingletonSeparatedList(
                                                                                                    VariableDeclarator(Identifier("items"))
                                                                                                        .WithInitializer(
                                                                                                                         EqualsValueClause(
                                                                                                                                           callOfBacking))))));

                var mapTo = GenericName(Identifier("MapTo"))
                    .WithTypeArgumentList(
                                          TypeArgumentList(
                                                           SeparatedList
                                                           <TypeSyntax
                                                           >(
                                                             new string[]
                                                                 {
                                                                     $"composite_{classDeclaration}",
                                                                     $"composite_{classDeclaration}_data",
                                                                     $"{classDeclaration}_published"
                                                                 }
                                                                 .Concat(relatesTo.Select(x => $"{x}_published"))
                                                                 .Select(x => IdentifierName(x))
                                                                 .Select<IdentifierNameSyntax, SyntaxNodeOrToken>(x => x)
                                                                 .SeparatedBy(Token(SyntaxKind.CommaToken))
                                                                 .ToArray())));

                listOfCalls.Add(ReturnStatement(
                                                InvocationExpression(
                                                                     MemberAccessExpression(
                                                                                            SyntaxKind.SimpleMemberAccessExpression,
                                                                                            IdentifierName("items"),
                                                                                            IdentifierName("Select")))
                                                    .WithArgumentList(
                                                                      ArgumentList(
                                                                                   SingletonSeparatedList(
                                                                                                          Argument(
                                                                                                                   this
                                                                                                                       .LambdaExpression(
                                                                                                                                         classDeclaration,
                                                                                                                                         relatesToCount,
                                                                                                                                         mapTo)))))));
                items.Add(this.MethodWithBody(
                                              new[] {SyntaxKind.PublicKeyword, SyntaxKind.AsyncKeyword},
                                              this.TypeOf(
                                                          new string[] {nameof(Task), "IEnumerable"},
                                                          this.FatTypeOf("IObjectEntityRow", new[] {$"composite_{classDeclaration}"})),
                                              "FindPublishedItems",
                                              listOfCalls.ToArray(),
                                              Tuple.Create($"composite_{classDeclaration}_search_data", "parameters")));

                items.Add(this.MethodWithBody(
                                              new[] {SyntaxKind.PublicKeyword, SyntaxKind.AsyncKeyword},
                                              this.TypeOf(
                                                          new string[] {nameof(Task), "IEnumerable"},
                                                          this.FatTypeOf("IObjectEntityRow", new[] {$"composite_{classDeclaration}"})),
                                              "FindPublishedItems",
                                              listOfCalls.ToArray(),
                                              Tuple.Create($"composite_{classDeclaration}_search_data", "parameters"),
                                              Tuple.Create($"Base2art.Data.EventSourcing.Modeling.IndexPagination", "pagination")));

                
                items.Add(this.MethodWithBody(
                                              new[] {SyntaxKind.PublicKeyword, SyntaxKind.AsyncKeyword},
                                              this.TypeOf(
                                                          new string[] {nameof(Task), "IEnumerable"},
                                                          this.FatTypeOf("IObjectEntityRow", new[] {$"composite_{classDeclaration}"})),
                                              "FindPublishedItems",
                                              listOfCalls.ToArray(),
                                              Tuple.Create($"composite_{classDeclaration}_search_data", "parameters"),
                                              Tuple.Create($"Base2art.Data.EventSourcing.Modeling.OffsetPagination", "pagination")));

                /*
                            this.Method(
                                        "backingSearcher1",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "IEnumerable", "IObjectEntityRow", $"{classDeclaration}_published"),
                                        "FindPublishedItems",
                                        Tuple.Create($"{classDeclaration}_search_data", "parameters"),
                                        Tuple.Create($"Base2art.Data.EventSourcing.Modeling.IndexPagination", "pagination")),

                            this.Method(
                                        "backingSearcher1",
                                        SyntaxKind.PublicKeyword,
                                        this.TypeOf(nameof(Task), "IEnumerable", "IObjectEntityRow", $"{classDeclaration}_published"),
                                        "FindPublishedItems",
                                        Tuple.Create($"{classDeclaration}_search_data", "parameters"),
                                        Tuple.Create($"Base2art.Data.EventSourcing.Modeling.OffsetPagination", "pagination")),*/

                // <TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TObjectVersionTable>
                // {"Many".Repeat(relatesToCount)}
                items.Insert(0, this.Field(SyntaxKind.PrivateKeyword, this.FatTypeOf(
                                                                                     $"GenericIsolatedSearchingRelatedDataAccessReader",
                                                                                     new string[]
                                                                                         {
                                                                                             $"composite_{classDeclaration}_search_data",
                                                                                             $"{classDeclaration}_published",
                                                                                         }
                                                                                         .Concat(relatesTo.Select(y => $"{y}_published"))
                                                                                         .ToArray()),
                                           "backingSearcher1_1"));
            }

            items.Insert(0, this.Field(SyntaxKind.PrivateKeyword, this.FatTypeOf(
                                                                                 "GenericIsolatedDataAccess",
                                                                                 classDeclaration,
                                                                                 $"{classDeclaration}_object",
                                                                                 $"{classDeclaration}_version",
                                                                                 $"{classDeclaration}_publication"),
                                       "backingReader"));

            items.Insert(0, this.Field(SyntaxKind.PrivateKeyword, this.FatTypeOf(
                                                                                 "GenericIsolatedSearchingDataAccessReader",
                                                                                 $"{classDeclaration}_search_data",
                                                                                 $"{classDeclaration}_published"),
                                       "backingSearcher1"));
            items.Insert(0, this.Field(SyntaxKind.PrivateKeyword, this.FatTypeOf(
                                                                                 "Repairer",
                                                                                 $"{classDeclaration}",
                                                                                 $"{classDeclaration}_published",
                                                                                 $"{classDeclaration}_publication",
                                                                                 $"{classDeclaration}_version"),
                                       "repairer1"));

            items.Add(InterfaceDeclaration($"{classDeclaration}_object")
                      .WithBaseList(BaseList(SingletonSeparatedList<BaseTypeSyntax>(SimpleBaseType(IdentifierName("es_object")))))
                      .WithModifiers(TokenList(Token(SyntaxKind.PrivateKeyword))));

            items.Add(InterfaceDeclaration($"{classDeclaration}_version")
                      .WithBaseList(BaseList(SingletonSeparatedList<BaseTypeSyntax>(SimpleBaseType(IdentifierName("es_version")))))
                      .WithModifiers(TokenList(Token(SyntaxKind.PrivateKeyword))));

            items.Add(InterfaceDeclaration($"{classDeclaration}_publication")
                      .WithBaseList(BaseList(SingletonSeparatedList<BaseTypeSyntax>(SimpleBaseType(IdentifierName("es_version_publication")))))
                      .WithModifiers(TokenList(Token(SyntaxKind.PrivateKeyword))));

            // GenericIsolatedDataAccess<company, company_object, company_version, company_publication> backingReader;
            Tuple<string, string>[] parameters = new[]
                                                 {
                                                     Tuple.Create("IDataStore", "store"),
                                                     Tuple.Create("IDataAccessRepairQueue", "queue")
                                                 };

            var blocks = new List<StatementSyntax>();
            blocks.Add(ExpressionStatement(
                                           AssignmentExpression(
                                                                SyntaxKind.SimpleAssignmentExpression,
                                                                IdentifierName("backingReader"),
                                                                ObjectCreationExpression(this.FatTypeOf(
                                                                                                        "GenericIsolatedDataAccess",
                                                                                                        classDeclaration,
                                                                                                        $"{classDeclaration}_object",
                                                                                                        $"{classDeclaration}_version",
                                                                                                        $"{classDeclaration}_publication"))
                                                                    .WithArgumentList(new[]
                                                                                      {
                                                                                          Tuple.Create("null", "null")
                                                                                      }.Concat(parameters).ToArray().ArgumentListInt()))));

            blocks.Add(ExpressionStatement(AssignmentExpression(
                                                                SyntaxKind.SimpleAssignmentExpression,
                                                                IdentifierName("backingSearcher1"),
                                                                ObjectCreationExpression(this.FatTypeOf(
                                                                                                        "GenericIsolatedSearchingDataAccessReader",
                                                                                                        $"{classDeclaration}_search_data",
                                                                                                        $"{classDeclaration}_published"))
                                                                    .WithArgumentList(new[]
                                                                                      {
                                                                                          Tuple.Create("IDataStore", "store"),
                                                                                          Tuple.Create("null", "null"),
                                                                                      }.ArgumentListInt()))));

            blocks.Add(ExpressionStatement(AssignmentExpression(
                                                                SyntaxKind.SimpleAssignmentExpression,
                                                                IdentifierName("repairer1"),
                                                                ObjectCreationExpression(this.FatTypeOf(
                                                                                                        "Repairer",
                                                                                                        $"{classDeclaration}",
                                                                                                        $"{classDeclaration}_published",
                                                                                                        $"{classDeclaration}_publication",
                                                                                                        $"{classDeclaration}_version"))
                                                                    .WithArgumentList(new[]
                                                                                      {
                                                                                          Tuple.Create("IDataStore", "store"),
                                                                                          Tuple.Create("null", "null"),
                                                                                      }.ArgumentListInt()))));

            blocks.Add(ExpressionStatement(
                                           InvocationExpression(MemberAccessExpression(
                                                                                       SyntaxKind.SimpleMemberAccessExpression,
                                                                                       IdentifierName("queue"),
                                                                                       GenericName(Identifier("AddRepairer"))
                                                                                           .WithTypeArgumentList(TypeArgumentList(SingletonSeparatedList
                                                                                                                                  <
                                                                                                                                      TypeSyntax>(
                                                                                                                                                  IdentifierName(classDeclaration))))
                                                                                      ))
                                               .WithArgumentList(ArgumentList(SingletonSeparatedList(Argument(IdentifierName("repairer1")))))));

            if (relatesToCount >= 1)
            {
                var manyManyParms = new[]
                                    {
                                        Tuple.Create("IDataStore", "store"),
                                    }
                                    .Concat(relatesToPairs.Select(x => Tuple.Create<string, string>("null", $"x=>x.{x.Name}")))
                                    .Concat(new[]
                                            {
                                                Tuple.Create("null", "null"),
                                            })
                                    .ToArray()
                                    .ArgumentListInt();

                blocks.Add(ExpressionStatement(AssignmentExpression(
                                                                    SyntaxKind.SimpleAssignmentExpression,
                                                                    IdentifierName("backingSearcher1_1"),
                                                                    ObjectCreationExpression(this.FatTypeOf(
                                                                                                            $"GenericIsolatedSearchingRelatedDataAccessReader",
                                                                                                            new string[]
                                                                                                                {
                                                                                                                    $"composite_{classDeclaration}_search_data",
                                                                                                                    $"{classDeclaration}_published",
                                                                                                                }
                                                                                                                .Concat(relatesTo.Select(y =>
                                                                                                                                             $"{y}_published"))
                                                                                                                .ToArray()))
                                                                        .WithArgumentList(manyManyParms))));
            }

            items.Add(this.Ctor(
                                classDeclaration,
                                props,
                                Block(blocks.ToArray()),
                                parameters));

            return List<MemberDeclarationSyntax>(items.ToArray());
        }

        private SimpleLambdaExpressionSyntax LambdaExpression(string classDeclaration, int relatesToCount, GenericNameSyntax mapTo)
            => SimpleLambdaExpression(
                                      Parameter(Identifier("x")),
                                      InvocationExpression(
                                                           MemberAccessExpression(
                                                                                  SyntaxKind.SimpleMemberAccessExpression,
                                                                                  IdentifierName($"CompositeObjectEntityRowMapper{(relatesToCount + 1)}"),
                                                                                  mapTo))
                                          .WithArgumentList(
                                                            ArgumentList(
                                                                         SeparatedList<ArgumentSyntax>(
                                                                                                       new
                                                                                                           SyntaxNodeOrToken
                                                                                                           []
                                                                                                           {
                                                                                                               Argument(IdentifierName("x")),
                                                                                                           }.Concat(GetMemberIds(classDeclaration,
                                                                                                                                 relatesToCount))
                                                                                                      ))));

        private IEnumerable<SyntaxNodeOrToken> GetMemberIds(string classDeclaration, int relatesToCount)
        {
            List<SyntaxNodeOrToken> items = new List<SyntaxNodeOrToken>();

            items.Add(Token(SyntaxKind.CommaToken));
            items.Add(Argument(
                               LiteralExpression(
                                                 SyntaxKind.StringLiteralExpression,
                                                 Literal(classDeclaration))));

//            string[] lookup = new[]
//                              {
//                                  "First",
//                                  "Second",
//                                  "Third",
//                                  "Fourth",
//                                  "Fifth",
//                                  "Sixth",
//                                  "Seventh",
//                                  "Eight",
//                                  "Nineth",
//                              };

            for (int i = 0; i < relatesToCount; i++)
            {
                items.Add(Token(SyntaxKind.CommaToken));
                items.Add(MemberId("LookupId", i));
            }

            return items.ToArray();
        }

        private static ArgumentSyntax MemberId(string name, int parameter)
            => Argument(
                        InvocationExpression(
                                             MemberAccessExpression(
                                                                    SyntaxKind.SimpleMemberAccessExpression,
                                                                    MemberAccessExpression(
                                                                                           SyntaxKind.SimpleMemberAccessExpression,
                                                                                           ThisExpression(),
                                                                                           IdentifierName("backingSearcher1_1")),
                                                                    IdentifierName(name)))
                            .WithArgumentList(ArgumentList(
                                                           SingletonSeparatedList(
                                                                                  Argument(
                                                                                           LiteralExpression(
                                                                                                             SyntaxKind.NumericLiteralExpression,
                                                                                                             Literal(parameter)))))));

        protected override BaseListSyntax BaseTypes(string classDeclaration, PropertyId[] properties)
        {
            return BaseList(SeparatedList<BaseTypeSyntax>(
                                                          new SyntaxNodeOrToken[]
                                                          {
                                                              SimpleBaseType(IdentifierName($"I{classDeclaration.ToCamelCase()}Repository")),
                                                          }));
        }

        protected override SyntaxList<UsingDirectiveSyntax> Usings()
            => List(
                    new[]
                    {
                        UsingDirective(IdentifierName("System")),
                        UsingDirective(IdentifierName("System.Collections.Generic")),
                        UsingDirective(IdentifierName("System.Linq")),
                        UsingDirective(IdentifierName("System.Threading.Tasks")),
                        UsingDirective(IdentifierName("Base2art.Data.EventSourcing")),
                        UsingDirective(IdentifierName("Base2art.Data.EventSourcing.Persistence")),
                        UsingDirective(IdentifierName("Base2art.Data.EventSourcing.Persistence.Tables")),
                        UsingDirective(IdentifierName("Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem")),
                        UsingDirective(IdentifierName("Base2art.Data.EventSourcing.Modeling")),
                        UsingDirective(IdentifierName("Base2art.DataStorage")),
                    });
    }
}