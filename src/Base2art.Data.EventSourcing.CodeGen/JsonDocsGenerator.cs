namespace Base2art.Data.EventSourcing.CodeGen
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    public class JsonDocsGenerator : IGenerator
    {
        public GeneratorDataType DataType() => GeneratorDataType.Json;

        public KeyValuePair<string, string>? Generate(string @namespace, string classDeclarations, PropertyId[] properties)
        {
            return new KeyValuePair<string, string>(classDeclarations, JsonConvert.SerializeObject(properties, Formatting.Indented));
        }

        public string TypeName(string classDeclaration, PropertyId[] properties) => classDeclaration;

        public ContentType ContentType => ContentType.Json;
    }
}