namespace Base2art.Data.EventSourcing.CodeGen
{
    using System;
    using System.Collections.Generic;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;
    using Parameter = System.Reflection.Metadata.Parameter;

    public abstract class FileGeneratorBase : IGenerator
    {
        public virtual GeneratorDataType DataType() => GeneratorDataType.Unknown;

        public KeyValuePair<string, string>? Generate(string @namespace, string classDeclaration, PropertyId[] properties)
        {
            var className = this.TypeName(classDeclaration, properties);
            if (string.IsNullOrWhiteSpace(className))
            {
                return default;
            }

            var result = CompilationUnit()
                         .WithMembers(SingletonList<MemberDeclarationSyntax>(
                                                                             NamespaceDeclaration(IdentifierName(@namespace))
                                                                                 .WithLeadingTrivia(this.Headers())
                                                                                 .WithUsings(this.Usings())
                                                                                 .WithMembers(this.Types(className,
                                                                                                         classDeclaration,
                                                                                                         properties))))
                         .NormalizeWhitespace();

            return new KeyValuePair<string, string>(className, result.ToFullString());
        }

        protected abstract SyntaxList<UsingDirectiveSyntax> Usings();

        public abstract string TypeName(string classDeclaration, PropertyId[] properties);
        
        public ContentType ContentType => ContentType.CS;

        private SyntaxList<MemberDeclarationSyntax> Types(string className, string classDeclaration, PropertyId[] properties)
        {
            BaseListSyntax baseTypes = this.BaseTypes(classDeclaration, properties);

            var memberDeclarationSyntax = this.TypeDeclaration(className)
                                              .WithModifiers(TokenList(Token(this.AccessKeyword())))
                                              .WithMembers(this.Members(classDeclaration, properties));

            if (baseTypes != null && baseTypes.Types.Any())
            {
                memberDeclarationSyntax = memberDeclarationSyntax.WithBaseList(baseTypes).WithLeadingTrivia(LineFeed);
            }

            return SingletonList<MemberDeclarationSyntax>(memberDeclarationSyntax);
        }

        protected virtual SyntaxKind AccessKeyword() => SyntaxKind.PublicKeyword;

        protected virtual BaseListSyntax BaseTypes(string classDeclaration, PropertyId[] properties) => BaseList();

        protected abstract TypeDeclarationSyntax TypeDeclaration(string className);

//        protected abstract SyntaxList<MemberDeclarationSyntax> Members(PropertyId[] properties);
        protected virtual SyntaxList<MemberDeclarationSyntax> Members(string classDeclaration, PropertyId[] props)
            => List<MemberDeclarationSyntax>();
    }
}