namespace Base2art.Data.EventSourcing.CodeGen
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection.Metadata;
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using Persistence.Tables;
    using Search;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class CompositeSearchDataFileGenerator : FileGeneratorBase
    {
        public override GeneratorDataType DataType() => GeneratorDataType.Search;

        public override string TypeName(string classDeclaration, PropertyId[] properties)
        {
            if (properties.Any(x => !string.IsNullOrWhiteSpace(x.RelatesTo)))
            {
                return $"composite_{classDeclaration}_search_data";
            }

            return null;
        }

        protected override TypeDeclarationSyntax TypeDeclaration(string className)
            => ClassDeclaration(className);

        protected override SyntaxList<MemberDeclarationSyntax> Members(string classDeclaration, PropertyId[] props)
        {
            return List(props.Where(x => !string.IsNullOrWhiteSpace(x.RelatesTo))
                             .Select(this.Map)
                             .Concat(new[] {this.GetSetProperty(classDeclaration, $"{classDeclaration}_search_data")})
                             .Where(x => x != null)
                             .ToArray());
        }

        private MemberDeclarationSyntax Map(PropertyId propertyId)
        {
            var mapType = this.MapType(propertyId.Type, propertyId.RelatesTo);
            var propertyIdName = this.MapName(propertyId.RelatesTo, propertyId.Name);

            return string.IsNullOrWhiteSpace(mapType)
                       ? null
                       : this.GetSetProperty(propertyIdName, mapType);
        }

        private string MapName(string propertyRelatesTo, string propertyIdName)
        {
            if (propertyIdName.EndsWith("_id", StringComparison.OrdinalIgnoreCase))
            {
                return propertyIdName.Substring(0, propertyIdName.Length - 3);
            }
//            if (propertyIdName.StartsWith(propertyRelatesTo, StringComparison.OrdinalIgnoreCase)
//                && propertyIdName.EndsWith("_id", StringComparison.OrdinalIgnoreCase))
//            {
//                return propertyIdName.Substring(0, propertyIdName.Length - 3);
//            }
//
//            if (propertyIdName.StartsWith(propertyRelatesTo, StringComparison.OrdinalIgnoreCase)
//                && propertyIdName.EndsWith("id", StringComparison.OrdinalIgnoreCase))
//            {
//                return propertyIdName.Substring(0, propertyIdName.Length - 2);
//            }

            return propertyIdName;
        }

        private string MapType(string propertyIdType, object propertyIdRelatesTo) => $"{propertyIdRelatesTo}_search_data";

        protected override SyntaxList<UsingDirectiveSyntax> Usings() =>
            SyntaxFactory.List(
                               new[]
                               {
                                   SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("System")),
                                   SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("Base2art.Data.EventSourcing.Persistence")),
                                   SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("Base2art.Data.EventSourcing.Persistence.Tables")),
                                   SyntaxFactory.UsingDirective(SyntaxFactory.IdentifierName("Base2art.Data.EventSourcing.Search"))
                               });
    }
}