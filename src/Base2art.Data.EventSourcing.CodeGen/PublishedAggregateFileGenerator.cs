namespace Base2art.Data.EventSourcing.CodeGen
{
    using Microsoft.CodeAnalysis;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;
    using Persistence.Tables;
    using static Microsoft.CodeAnalysis.CSharp.SyntaxFactory;

    public class PublishedAggregateFileGenerator : FileGeneratorBase, IGenerator
    {
        
        public override GeneratorDataType DataType() => GeneratorDataType.Table;

        public override string TypeName(string classDeclaration, PropertyId[] properties) => $"{classDeclaration}_published";

        protected override TypeDeclarationSyntax TypeDeclaration(string className)
            => InterfaceDeclaration(className);

//
        protected override BaseListSyntax BaseTypes(string classDeclaration, PropertyId[] properties)
            => BaseList(SeparatedList<BaseTypeSyntax>(
                                                      new SyntaxNodeOrToken[]
                                                      {
                                                          SimpleBaseType(IdentifierName(classDeclaration)),
                                                          Token(SyntaxKind.CommaToken),
                                                          SimpleBaseType(IdentifierName(nameof(es_agg_published)))
                                                      }));

        protected override SyntaxList<UsingDirectiveSyntax> Usings() =>
            List(
                               new[]
                               {
                                   UsingDirective(SyntaxFactory.IdentifierName("System")),
                                   UsingDirective(SyntaxFactory.IdentifierName("Base2art.Data.EventSourcing.Persistence")),
                                   UsingDirective(SyntaxFactory.IdentifierName("Base2art.Data.EventSourcing.Persistence.Tables"))
                               });
    }
}