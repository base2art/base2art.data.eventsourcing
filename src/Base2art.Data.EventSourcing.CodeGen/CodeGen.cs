﻿namespace Base2art.Data.EventSourcing.CodeGen
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.CodeAnalysis.CSharp;
    using Microsoft.CodeAnalysis.CSharp.Syntax;

    public class CodeGen
    {
        private readonly DirectoryInfo scanDirectory;
        private readonly DirectoryInfo outputDirectory;

        public CodeGen(DirectoryInfo scanDirectory, DirectoryInfo outputDirectory)
        {
            this.scanDirectory = scanDirectory;
            this.outputDirectory = outputDirectory;
        }

        public IEnumerable<IGenerator> Generators()
        {
            yield return new JsonDocsGenerator();
//            yield return new RiotJsListGenerator();
            yield return new CompositeDataFileGenerator();
            yield return new CompositeDataImplFileGenerator();
            yield return new DataFileGenerator();
            yield return new PublishedAggregateFileGenerator();
            yield return new SearchDataFileGenerator();
            yield return new CompositeSearchDataFileGenerator();
            yield return new RepositoryInterfaceFileGenerator();
            yield return new RepositoryImplementationFileGenerator();
        }

        public async Task Process(bool testRun)
        {
            var files = this.scanDirectory.GetFiles("*.cs", SearchOption.AllDirectories)
                            .Where(x => !x.Name.EndsWith(".generated.cs"));

            foreach (var file in files)
            {
                Console.WriteLine(file.FullName);
                await this.ProcessFile(file, testRun);
            }
        }

        private async Task ProcessFile(FileInfo file, bool testRun)
        {
            var code = File.ReadAllText(file.FullName);
            var tree = CSharpSyntaxTree.ParseText(code);

            // Get the root CompilationUnitSyntax.
            var root = await tree.GetRootAsync().ConfigureAwait(false) as CompilationUnitSyntax;

            // Get the namespace declaration.
            var oldNamespace = root.Members.OfType<NamespaceDeclarationSyntax>()
                                   .Single();

            var ns = oldNamespace.Name.ToString();
            // Get all class declarations inside the namespace.
            var classDeclarations = oldNamespace.Members.OfType<TypeDeclarationSyntax>()
                                                .Select(x => Tuple.Create(x.Identifier.Text, this.GetProperties(x)))
                                                .ToArray();

            foreach (var classDeclaration in classDeclarations)
            {
                foreach (var generator in this.Generators())
                {
                    var contentWrapper = generator.Generate(ns, classDeclaration.Item1, classDeclaration.Item2);

                    if (contentWrapper.HasValue)
                    {
                        var content = contentWrapper.Value;
                        var outDir = Path.Combine(this.outputDirectory.FullName, generator.DataType().ToString("G").Replace("_", "/"));
                        var outDirInfo = Directory.CreateDirectory(outDir);
                        var path = Path.Combine(outDirInfo.FullName,
                                                $"{content.Key}.generated.{generator.ContentType.ToString("G").Replace("_", ".").ToLowerInvariant()}");
                        if (!testRun)
                        {
                            File.WriteAllText(path, content.Value);
                        }
                    }
                }
            }

            // Output new code to the console.
//            Console.WriteLine(newCode);
//            Console.WriteLine("Namespace replaced...");
        }

        private PropertyId[] GetProperties(TypeDeclarationSyntax typeDeclarationSyntax)
        {
            var prop = typeDeclarationSyntax.Members.OfType<PropertyDeclarationSyntax>()
                                            .Select(x => this.Map(x))
                                            .Where(x => x != null);

            return prop.ToArray();
//           typeDeclarationSyntax.Members.OfType<PropertyDeclarationSyntax>()
//                                .Select(x=>x.Type)
        }

        private PropertyId Map(PropertyDeclarationSyntax prop)
        {
            var type = prop.Type;
            var ident = prop.Identifier;
            var hasGet = prop.AccessorList.Accessors.Any(x => x.Keyword.Text == "get");
            var hasSet = prop.AccessorList.Accessors.Any(x => x.Keyword.Text == "set");
            var relatesTo = prop.AttributeLists.SelectMany(x => x.Attributes
                                                                 .Where(y => y.Name.ToFullString() == "RelatesTo")
                                                                 .Select(y => y.ArgumentList.Arguments.FirstOrDefault()))
                                .FirstOrDefault(x => x != null);

            var invokeExpression = relatesTo?.Expression as InvocationExpressionSyntax;
            var literalExpression = relatesTo?.Expression as LiteralExpressionSyntax;
            var ivokeName = invokeExpression?.ArgumentList?.Arguments.FirstOrDefault()?.ToString();
            var literalName = literalExpression?.Token.ValueText;

            Console.WriteLine(prop);
            return new PropertyId
                   {
                       Type = type.ToString(),
                       Name = ident.ToString(),
                       HasGet = hasGet,
                       HasSet = hasSet,
                       RelatesTo = ivokeName ?? literalName
                   };
        }
    }
}