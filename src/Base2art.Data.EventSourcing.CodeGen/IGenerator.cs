namespace Base2art.Data.EventSourcing.CodeGen
{
    using System.Collections.Generic;

    public interface IGenerator
    {
        GeneratorDataType DataType();

        KeyValuePair<string, string>? Generate(string @namespace, string classDeclarations, PropertyId[] properties);

        string TypeName(string classDeclaration, PropertyId[] properties);

        ContentType ContentType { get; }
    }
}