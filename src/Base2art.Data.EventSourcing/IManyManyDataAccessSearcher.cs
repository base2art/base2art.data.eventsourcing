namespace Base2art.Data.EventSourcing
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Modeling;

    public interface IRelatedDataAccessSearcher<in TParameters, TObjectEntityData, TFirstEntity>
    {
        Task<IEnumerable<IObjectEntityRow<Tuple<TObjectEntityData, TFirstEntity>>>> FindPublishedItems(TParameters parameters);
    }

    public interface IRelatedDataAccessSearcher<in TParameters, TObjectEntityData, TFirstEntity, TSecondEntity>
    {
        Task<IEnumerable<IObjectEntityRow<Tuple<TObjectEntityData, TFirstEntity, TSecondEntity>>>> FindPublishedItems(TParameters parameters);
    }

    public interface IRelatedDataAccessSearcher<in TParameters, TObjectEntityData, TFirstEntity, TSecondEntity, TThirdEntity>
    {
        Task<IEnumerable<IObjectEntityRow<Tuple<TObjectEntityData, TFirstEntity, TSecondEntity, TThirdEntity>>>> FindPublishedItems(
            TParameters parameters);
    }

    public interface IRelatedDataAccessSearcher<in TParameters, TObjectEntityData, TFirstEntity, TSecondEntity, TThirdEntity, TFourthEntity>
    {
        Task<IEnumerable<IObjectEntityRow<Tuple<TObjectEntityData, TFirstEntity, TSecondEntity, TThirdEntity, TFourthEntity>>>> FindPublishedItems(
            TParameters parameters);
    }

    public interface IRelatedDataAccessSearcher<in TParameters, TObjectEntityData, TFirstEntity, TSecondEntity, TThirdEntity, TFourthEntity,
                                                TFifthEntity>
    {
        Task<IEnumerable<IObjectEntityRow<Tuple<TObjectEntityData, TFirstEntity, TSecondEntity, TThirdEntity, TFourthEntity, TFifthEntity>>>>
            FindPublishedItems(TParameters parameters);
    }

    public interface IRelatedDataAccessSearcher<in TParameters, TObjectEntityData, TFirstEntity, TSecondEntity, TThirdEntity, TFourthEntity,
                                                TFifthEntity, TSixthEntity>
    {
        Task<IEnumerable<IObjectEntityRow<
                Tuple<TObjectEntityData, TFirstEntity, TSecondEntity, TThirdEntity, TFourthEntity, TFifthEntity, TSixthEntity>>>>
            FindPublishedItems(TParameters parameters);
    }
}