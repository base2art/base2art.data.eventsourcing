namespace Base2art.Data.EventSourcing
{
    using System;
    using System.Threading.Tasks;
    using Persistence;
    using Persistence.IsolatedObjectSystem;
    using Persistence.Tables;

    public interface IDataAccessRepairQueue
    {
        Task PushRepairAsync(Guid id, Type dataType);

        void AddRepairer<T>(IRepairer repairer);
    }
}