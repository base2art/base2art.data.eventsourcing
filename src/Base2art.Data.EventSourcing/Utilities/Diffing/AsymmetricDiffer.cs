namespace Base2art.Data.EventSourcing.Utilities.Diffing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AsymmetricDiffer<T1, T2>
    {
        private readonly T1[] old;
        private readonly T2 @new;

        public AsymmetricDiffer(T1[] old, T2 @new)
        {
            this.old = old;
            this.@new = @new;
        }

        public AsymmetricCompareResult<T1, TPropType> Compare<TPropType>(Func<T1, TPropType> func1, Func<T2, IEnumerable<TPropType>> func2)
            where TPropType : IComparable<TPropType>
        {
            return new AsymmetricCompareResult<T1, TPropType>(
                                                              this.@old.Select(x => Tuple.Create(func1(x), x)).Distinct().ToArray(),
//                                                                  this.@old(x => Tuple.Create(func2(x), x)).Distinct().ToArray())
//                                                                  func1(this.@old).Distinct().ToArray(),
                                                              func2(this.@new).Distinct().ToArray());
//                                                                  this.@new(x => Tuple.Create(func2(x), x)).Distinct().ToArray());
        }
    }
}