namespace Base2art.Data.EventSourcing.Utilities.Diffing
{
    using System.Linq;

    public class SymmetricCompareResult<T>
    {
        private readonly T[] old;
        private readonly T[] @new;

        public SymmetricCompareResult(T[] old, T[] @new)
        {
            this.old = old;
            this.@new = @new;
        }

        public T[] Adds => this.@new.Except(this.old).ToArray();
        public T[] Removes => this.old.Except(this.@new).ToArray();
    }
}