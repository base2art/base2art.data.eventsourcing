namespace Base2art.Data.EventSourcing.Utilities.Diffing
{
    using System;
    using System.Linq;

    public class AsymmetricCompareResult<T1, TResult>
        where TResult : IComparable<TResult>
    {
        private readonly Tuple<TResult, T1>[] old;
        private readonly TResult[] @new;

        public AsymmetricCompareResult(Tuple<TResult, T1>[] old, TResult[] @new)
        {
            this.old = old;
            this.@new = @new;
        }

        public TResult[] Adds
        {
            get
            {
                var older = this.old.Select(y => y.Item1);
                return this.@new.Where(x => x.NotIn(older)).ToArray();
            }
        }

        public T1[] Removes
        {
            get
            {
//             var newer = this.@new.Select(y => y.Item1)
                var removes = this.old
                                  .Where(x => x.Item1.NotIn(this.@new))
                                  .Select(x => x.Item2)
                                  .ToArray();

                return removes;
            }
        }

        //this.@new.Where(x => x.Item1.NotIn(this.old)).Select(x => x.Item2).ToArray();
    }
}