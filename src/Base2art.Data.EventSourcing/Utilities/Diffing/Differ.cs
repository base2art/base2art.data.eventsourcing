namespace Base2art.Data.EventSourcing.Utilities.Diffing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class Differ
    {
        public static SymmetricDiffer<T> CreateSymmetric<T>(T old, T @new)
        {
            return new SymmetricDiffer<T>(old, @new);
        }

        public static AsymmetricDiffer<T1, T2> CreateAsymmetric<T1, T2>(T1[] old, T2 @new)
        {
            return new AsymmetricDiffer<T1, T2>(old, @new);
        }

        public static bool In<T>(this T value, IEnumerable<T> search)
            where T : IComparable<T>
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (search == null)
            {
                throw new ArgumentNullException(nameof(search));
            }

            return search.Any(x => value.CompareTo(x) == 0);
        }

        public static bool NotIn<T>(this T value, IEnumerable<T> search)
            where T : IComparable<T>
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (search == null)
            {
                throw new ArgumentNullException(nameof(search));
            }

            return search.All(x => value.CompareTo(x) != 0);
        }
    }
}