namespace Base2art.Data.EventSourcing.Utilities.Diffing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class SymmetricDiffer<T>
    {
        private readonly T old;
        private readonly T @new;

        public SymmetricDiffer(T old, T @new)
        {
            this.old = old;
            this.@new = @new;
        }

        public SymmetricCompareResult<TPropType> Compare<TPropType>(Func<T, IEnumerable<TPropType>> func)
        {
            return new SymmetricCompareResult<TPropType>(
                                                func(this.old).Distinct().ToArray(),
                                                func(this.@new).Distinct().ToArray());
        }
    }
}