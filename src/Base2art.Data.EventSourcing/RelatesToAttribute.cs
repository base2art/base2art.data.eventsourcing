namespace Base2art.Data.EventSourcing
{
    using System;

    public class RelatesToAttribute : Attribute
    {
        private readonly string tableName;

        public RelatesToAttribute(string tableName)
        {
            this.tableName = tableName;
        }
    }
}