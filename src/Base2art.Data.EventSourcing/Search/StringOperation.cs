namespace Base2art.Data.EventSourcing.Search
{
    public enum StringOperation
    {
        Like,
        Equal,
        NotEqual
    }
}