namespace Base2art.Data.EventSourcing.Search
{
    public class FieldSearchParameters<T, TOperation>
    {
        public TOperation Operation { get; set; }
        public T Value { get; set; }
    }
}