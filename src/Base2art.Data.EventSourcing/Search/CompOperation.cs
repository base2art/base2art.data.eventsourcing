namespace Base2art.Data.EventSourcing.Search
{
    public enum CompOperation
    {
        GreaterThan,
        LessThan,
        GreaterThanOrEqual,
        LessThanOrEqual,
        Equal,
        NotEqual
    }
}