namespace Base2art.Data.EventSourcing.Search
{
    using System;

    public class GuidSearchParameters : FieldSearchParameters<Guid?, EqualityOperation>
    {
    }
}