namespace Base2art.Data.EventSourcing.Search
{
    using System;

    public class DateTimeOffsetSearchParameters : NumberSearchParameters<DateTimeOffset>
    {
    }
}