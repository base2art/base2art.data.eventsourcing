namespace Base2art.Data.EventSourcing
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Threading.Tasks;

    public static class DataAccessWriter
    {
        internal static PropertyInfo GetInheritedProperty(this Type type, string propertyName)
        {
            PropertyInfo prop = type.GetProperty(propertyName);
            if (prop != null)
            {
                return prop;
            }

            var baseTypesAndInterfaces = new List<Type>();
            var typeInfo = type.GetTypeInfo();
            if (typeInfo.BaseType != null)
            {
                baseTypesAndInterfaces.Add(typeInfo.BaseType);
            }

            baseTypesAndInterfaces.AddRange(type.GetInterfaces());

            foreach (Type t in baseTypesAndInterfaces)
            {
                var prop1 = t.GetInheritedProperty(propertyName);
                if (prop1 != null)
                {
                    return prop1;
                }
            }

            return null;
        }

        public static async Task<ObjectVersionId> CreateAndPublishObject<TObjectEntityData>(
            this IDataAccessWriter<TObjectEntityData> writer,
            TObjectEntityData data,
            Guid userId)
        {
            var id = await writer.CreateObject(data, userId);
            await writer.PublishVersion(id.ObjectId, id.VersionId, userId);
            return id;
        }

        public static Task<ObjectVersionId> InsertAndPublishVersion<TObjectEntityData>(
            this IDataAccessWriter<TObjectEntityData> writer,
            TObjectEntityData data,
            ObjectVersionId id,
            Guid userId)
        {
            return writer.InsertAndPublishVersion(id.ObjectId, data, id.VersionId, userId);
        }

        public static async Task<ObjectVersionId> InsertAndPublishVersion<TObjectEntityData>(
            this IDataAccessWriter<TObjectEntityData> writer,
            Guid objectId,
            TObjectEntityData data,
            Guid versionId,
            Guid userId)
        {
            versionId = await writer.InsertVersion(objectId, data, versionId, userId);
            await writer.PublishVersion(objectId, versionId, userId);
            return new ObjectVersionId(objectId, versionId);
        }
    }
}