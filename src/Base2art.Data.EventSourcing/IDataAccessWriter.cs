namespace Base2art.Data.EventSourcing
{
    using System;
    using System.Threading.Tasks;

    public interface IDataAccessWriter<in TObjectEntityData>
    {
        Task<ObjectVersionId> CreateObject(TObjectEntityData data, Guid userId);

        Task<Guid> InsertVersion(Guid id, TObjectEntityData data, Guid previousVersion, Guid userId);

        Task UnpublishObject(Guid id, Guid userId);

        Task PublishVersion(Guid id, Guid version, Guid userId);
    }
}