namespace Base2art.Data.EventSourcing.Modeling
{
    public interface IObjectEntity<out TObjectEntityData> : IIdentifiable, IVersionable
    {
        TObjectEntityData Data { get; }
    }
}