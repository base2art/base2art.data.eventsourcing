namespace Base2art.Data.EventSourcing.Modeling
{
    public interface IObjectEntityRow<out TObjectEntityData> : IIdentifiable, IVersioned
    {
        TObjectEntityData Data { get; }
    }
}