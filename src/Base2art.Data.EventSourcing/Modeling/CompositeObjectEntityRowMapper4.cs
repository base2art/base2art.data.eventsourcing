namespace Base2art.Data.EventSourcing.Modeling
{
    using System;
    using System.Linq;
    using System.Reflection;

    public static class CompositeObjectEntityRowMapper4
    {
        public static IObjectEntityRow<TReturn> MapTo<TReturn, TActual, T2, T3, T4, T5>(
            IObjectEntityRow<Tuple<T2, T3, T4, T5>> objectEntityRow,
            string primaryId,
            string firstId,
            string secondId,
            string thirdId)
            where TActual : TReturn, new()
        {
            return new CompositeObjectEntityRow<TReturn, T2, T3, T4, T5>(new TActual(), objectEntityRow, primaryId, firstId, secondId, thirdId);
        }

        private class CompositeObjectEntityRow<T, TFirst, TSecond, TThird, TFourth> : IObjectEntityRow<T>
        {
            private readonly IObjectEntityRow<Tuple<TFirst, TSecond, TThird, TFourth>> objectEntityRow;

            public CompositeObjectEntityRow(
                T returnObj,
                IObjectEntityRow<Tuple<TFirst, TSecond, TThird, TFourth>> objectEntityRow,
                string primaryId,
                string firstId,
                string secondId,
                string thirdId)
            {
                this.objectEntityRow = objectEntityRow;

                var type = returnObj.GetType();
                var properties = type.GetTypeInfo().DeclaredProperties.ToArray();

                this.SetValue(returnObj, properties, primaryId, objectEntityRow.Data.Item1);
                this.SetValue(returnObj, properties, firstId, objectEntityRow.Data.Item2);
                this.SetValue(returnObj, properties, secondId, objectEntityRow.Data.Item3);
                this.SetValue(returnObj, properties, thirdId, objectEntityRow.Data.Item4);

                this.Data = returnObj;
            }

            private void SetValue<TSomething>(object obj, PropertyInfo[] properties, string propId, TSomething dataItem)
            {
                var prop = properties.FirstOrDefault(x => x.Name == propId)
                           ?? properties.FirstOrDefault(x => $"{x.Name}_id" == propId);
                if (prop != null)
                {
                    var setMethod = prop.SetMethod;
                    setMethod.Invoke(obj, new object[] {dataItem});
                }
            }

            public Guid Id => this.objectEntityRow.Id;
            public IVersionData RepresentedVersion => this.objectEntityRow.RepresentedVersion;
            public T Data { get; private set; }
        }
    }
}