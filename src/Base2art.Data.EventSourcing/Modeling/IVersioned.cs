namespace Base2art.Data.EventSourcing.Modeling
{
    public interface IVersioned
    {
        IVersionData RepresentedVersion { get; }
    }
}