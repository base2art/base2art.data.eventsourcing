namespace Base2art.Data.EventSourcing.Modeling
{
    using System;

    public interface IVersionData
    {
        int VersionNumber { get; }
        Guid VersionId { get; }
        Guid? CreatedFrom { get; }
        Guid CreatedBy { get; }
        DateTime CreatedAt { get; }
    }
}