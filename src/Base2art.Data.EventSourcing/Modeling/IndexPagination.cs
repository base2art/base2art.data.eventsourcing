namespace Base2art.Data.EventSourcing.Modeling
{
    public class IndexPagination
    {
        public int PageIndex { get; set; } = 0;

        public int PageSize { get; set; } = 10;
    }
}