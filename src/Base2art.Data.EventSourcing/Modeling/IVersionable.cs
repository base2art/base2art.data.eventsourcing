namespace Base2art.Data.EventSourcing.Modeling
{
    public interface IVersionable : IVersioned
    {
        IVersionData[] Versions { get; }
    }
}