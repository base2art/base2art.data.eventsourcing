namespace Base2art.Data.EventSourcing.Modeling
{
    using System;

    public interface IIdentifiable
    {
        Guid Id { get; }
    }
}