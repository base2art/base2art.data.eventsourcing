namespace Base2art.Data.EventSourcing.Modeling
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;

    public static class CompositeObjectEntityRowMapper2
    {
        public static IObjectEntityRow<TReturn> MapTo<TReturn, TActual, T2, T3>(
            IObjectEntityRow<Tuple<T2, T3>> objectEntityRow,
            string primaryId,
            string firstId)
            where TActual : TReturn, new()
        {
            return new CompositeObjectEntityRow<TReturn, T2, T3>(new TActual(), objectEntityRow, primaryId, firstId);
        }

        private class CompositeObjectEntityRow<T, TFirst, TSecond> : IObjectEntityRow<T>
        {
            private readonly IObjectEntityRow<Tuple<TFirst, TSecond>> objectEntityRow;

            public CompositeObjectEntityRow(
                T returnObj,
                IObjectEntityRow<Tuple<TFirst, TSecond>> objectEntityRow,
                string primaryId,
                string firstId)
            {
                this.objectEntityRow = objectEntityRow;

                var type = returnObj.GetType();
                var properties = type.GetTypeInfo().DeclaredProperties.ToArray();

                this.SetValue(returnObj, properties, primaryId, objectEntityRow.Data.Item1);
                this.SetValue(returnObj, properties, firstId, objectEntityRow.Data.Item2);

                this.Data = returnObj;
            }

            private void SetValue<TSomething>(object obj, PropertyInfo[] properties, string propId, TSomething dataItem)
            {
                var prop = properties.FirstOrDefault(x => x.Name == propId)
                           ?? properties.FirstOrDefault(x => $"{x.Name}_id" == propId);
                if (prop != null)
                {
                    var setMethod = prop.SetMethod;
                    setMethod.Invoke(obj, new object[] {dataItem});
                }
            }

            public Guid Id => this.objectEntityRow.Id;
            public IVersionData RepresentedVersion => this.objectEntityRow.RepresentedVersion;
            public T Data { get; private set; }
        }
    }
}