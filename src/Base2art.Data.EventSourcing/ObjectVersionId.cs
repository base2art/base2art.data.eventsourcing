namespace Base2art.Data.EventSourcing
{
    using System;

    public struct ObjectVersionId
    {
        public ObjectVersionId(Guid id, Guid versionId)
        {
            this.ObjectId = id;
            this.VersionId = versionId;
        }

        public Guid ObjectId { get; set; }
        public Guid VersionId { get; set; }
    }
}