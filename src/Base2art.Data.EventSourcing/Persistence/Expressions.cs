namespace Base2art.Data.EventSourcing.Persistence
{
    using System;
    using System.Linq.Expressions;

    public class Expressions
    {
        public static Expression<Func<T, T, bool>> Equal<T>()
        {
            return Expr<T>(Expression.Equal);
        }

        public static Expression<Func<T, T, bool>> NotEqual<T>()
        {
            return Expr<T>(Expression.NotEqual);
        }

        public static Expression<Func<T, T, bool>> GreaterThan<T>()
        {
            return Expr<T>(Expression.GreaterThan);
        }

        public static Expression<Func<T, T, bool>> GreaterThanOrEqual<T>()
        {
            return Expr<T>(Expression.GreaterThanOrEqual);
        }

        public static Expression<Func<T, T, bool>> LessThan<T>()
        {
            return Expr<T>(Expression.LessThan);
        }

        public static Expression<Func<T, T, bool>> LessThanOrEqual<T>()
        {
            return Expr<T>(Expression.LessThanOrEqual);
        }

        private static Expression<Func<T, T, bool>> Expr<T>(Func<ParameterExpression, ParameterExpression, BinaryExpression> op)
        {
            var paramA = Expression.Parameter(typeof(T), "a");
            var paramB = Expression.Parameter(typeof(T), "b");
            var body = op(paramA, paramB);
            return Expression.Lambda<Func<T, T, bool>>(body, paramA, paramB);
        }
    }
}