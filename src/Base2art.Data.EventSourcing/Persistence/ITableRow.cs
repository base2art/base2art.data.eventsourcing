namespace Base2art.Data.EventSourcing.Persistence
{
    using System;

    public interface ITableRow
    {
        Guid id { get; }
    }
}