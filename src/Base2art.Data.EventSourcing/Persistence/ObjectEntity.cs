namespace Base2art.Data.EventSourcing.Persistence
{
    using System;
    using System.Linq;
    using Modeling;

    public class ObjectEntity<T, TObjectTable, TObjectVersionTable, TPublicationTable> : IObjectEntity<T>
        where T : ITableRow
        where TObjectTable : Tables.es_object
        where TObjectVersionTable : Tables.es_version
        where TPublicationTable : class, Tables.es_version_publication
    {
        public Guid Id { get; }
        public T Data { get; }
        public IVersionData[] Versions { get; }
        public IVersionData RepresentedVersion { get; }

        internal ObjectEntity(
            TObjectTable obj,
            T versionData,
            TObjectVersionTable[] allVersions,
            TObjectVersionTable currentVersion)
        {
            this.Versions = allVersions.Select(x => new VersionData(x))
                                       .Select<VersionData, IVersionData>(x => x)
                                       .ToArray();
            this.Id = obj.object_id;
            this.Data = versionData;
            this.RepresentedVersion = new VersionData(currentVersion);
        }

        private class VersionData : IVersionData
        {
            public VersionData(TObjectVersionTable esVersion)
            {
                this.CreatedAt = esVersion.created_at;
                this.CreatedBy = esVersion.created_by;
                this.CreatedFrom = esVersion.created_from;
                this.VersionId = esVersion.version_id;
                this.VersionNumber = esVersion.version_number;
            }

            public int VersionNumber { get; }
            public Guid VersionId { get; }
            public Guid? CreatedFrom { get; }
            public Guid CreatedBy { get; }
            public DateTime CreatedAt { get; }
        }
    }
}