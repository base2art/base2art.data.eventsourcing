namespace Base2art.Data.EventSourcing.Persistence
{
    using DataStorage.DataManipulation.Builders;
    using Tables;

    public interface IRelatedQueryFilter<in TParameters, TJoinAgg, TFirstAgg>
        where TJoinAgg : ITableRow, es_agg_published
        where TFirstAgg : ITableRow, es_agg_published
    {
        void Where1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters);

        void Where2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters);
    }

    public interface IRelatedQueryFilter<in TParameters, TJoinAgg, TFirstAgg, TSecondAgg>
        where TJoinAgg : ITableRow, es_agg_published
        where TFirstAgg : ITableRow, es_agg_published
        where TSecondAgg : ITableRow, es_agg_published
    {
        void Where1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters);

        void Where2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters);

        void Where3(IWhereClauseBuilder<TSecondAgg> rs, TParameters parameters);
    }

    public interface IRelatedQueryFilter<in TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg>
        where TJoinAgg : ITableRow, es_agg_published
        where TFirstAgg : ITableRow, es_agg_published
        where TSecondAgg : ITableRow, es_agg_published
        where TThirdAgg : ITableRow, es_agg_published
    {
        void Where1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters);

        void Where2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters);

        void Where3(IWhereClauseBuilder<TSecondAgg> rs, TParameters parameters);

        void Where4(IWhereClauseBuilder<TThirdAgg> rs, TParameters parameters);
    }

    public interface IRelatedQueryFilter<in TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourthAgg>
        where TJoinAgg : ITableRow, es_agg_published
        where TFirstAgg : ITableRow, es_agg_published
        where TSecondAgg : ITableRow, es_agg_published
        where TThirdAgg : ITableRow, es_agg_published
        where TFourthAgg : ITableRow, es_agg_published
    {
        void Where1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters);

        void Where2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters);

        void Where3(IWhereClauseBuilder<TSecondAgg> rs, TParameters parameters);

        void Where4(IWhereClauseBuilder<TThirdAgg> rs, TParameters parameters);

        void Where5(IWhereClauseBuilder<TFourthAgg> rs, TParameters parameters);
    }

    public interface IRelatedQueryFilter<in TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourthAgg, TFifthAgg>
        where TJoinAgg : ITableRow, es_agg_published
        where TFirstAgg : ITableRow, es_agg_published
        where TSecondAgg : ITableRow, es_agg_published
        where TThirdAgg : ITableRow, es_agg_published
        where TFourthAgg : ITableRow, es_agg_published
        where TFifthAgg : ITableRow, es_agg_published
    {
        void Where1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters);

        void Where2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters);

        void Where3(IWhereClauseBuilder<TSecondAgg> rs, TParameters parameters);

        void Where4(IWhereClauseBuilder<TThirdAgg> rs, TParameters parameters);

        void Where5(IWhereClauseBuilder<TFourthAgg> rs, TParameters parameters);

        void Where6(IWhereClauseBuilder<TFifthAgg> rs, TParameters parameters);
    }

    public interface IRelatedQueryFilter<in TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourthAgg, TFifthAgg, TSixthAgg>
        where TJoinAgg : ITableRow, es_agg_published
        where TFirstAgg : ITableRow, es_agg_published
        where TSecondAgg : ITableRow, es_agg_published
        where TThirdAgg : ITableRow, es_agg_published
        where TFourthAgg : ITableRow, es_agg_published
        where TFifthAgg : ITableRow, es_agg_published
        where TSixthAgg : ITableRow, es_agg_published
    {
        void Where1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters);

        void Where2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters);

        void Where3(IWhereClauseBuilder<TSecondAgg> rs, TParameters parameters);

        void Where4(IWhereClauseBuilder<TThirdAgg> rs, TParameters parameters);

        void Where5(IWhereClauseBuilder<TFourthAgg> rs, TParameters parameters);

        void Where6(IWhereClauseBuilder<TFifthAgg> rs, TParameters parameters);

        void Where7(IWhereClauseBuilder<TSixthAgg> rs, TParameters parameters);
    }
}