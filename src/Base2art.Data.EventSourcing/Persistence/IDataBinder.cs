namespace Base2art.Data.EventSourcing.Persistence
{
    using DataStorage.DataManipulation.Builders;

    public interface IDataBinder<T>
    {
        ISetListBuilder<T> Bind(ISetListBuilder<T> field, T data);
    }
}