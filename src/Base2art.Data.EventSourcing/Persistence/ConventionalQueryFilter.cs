namespace Base2art.Data.EventSourcing.Persistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using DataStorage.DataManipulation;
    using DataStorage.DataManipulation.Builders;
    using Search;

    public class ConventionalQueryFilter<TParameters, TObject> : IQueryFilter<TParameters, TObject>
    {
        public void Filter(IWhereClauseBuilder<TObject> rs, TParameters parameters)
        {
            if (parameters == null)
            {
                return;
            }

            var props = typeof(TParameters).GetProperties();

            Dictionary<Type, Action<PropertyInfo, object>> processor = new Dictionary<Type, Action<PropertyInfo, object>>();

            void AddProcStruct<TParm, TValue, TOp>(Func<TOp, Expression<Func<TValue, TValue, bool>>> lookup)
                where TParm : FieldSearchParameters<TValue, TOp>
                where TOp : struct
            {
                processor[typeof(TParm)] = (prop, x) => this.ProcessStruct<TValue, TOp>(
                                                                                        rs,
                                                                                        prop,
                                                                                        (FieldSearchParameters<TValue, TOp>) x,
                                                                                        lookup);
            }

            void AddProcClass<TParm, TValue, TOp>(Func<TOp, Expression<Func<TValue, TValue, bool>>> lookup)
                where TParm : FieldSearchParameters<TValue, TOp>
                where TValue : class
                where TOp : struct
            {
                processor[typeof(TParm)] = (prop, x) => this.ProcessClass<TValue, TOp>(
                                                                                       rs,
                                                                                       prop,
                                                                                       (FieldSearchParameters<TValue, TOp>) x,
                                                                                       lookup);
            }

            AddProcStruct<BooleanSearchParameters, bool?, EqualityOperation>(this.GetExprEq<bool?>);
            AddProcStruct<ByteSearchParameters, byte?, CompOperation>(this.GetExprComp<byte?>);
            AddProcStruct<DateTimeSearchParameters, DateTime?, CompOperation>(this.GetExprComp<DateTime?>);
            AddProcStruct<DateTimeOffsetSearchParameters, DateTimeOffset?, CompOperation>(this.GetExprComp<DateTimeOffset?>);
            AddProcStruct<DecimalSearchParameters, decimal?, CompOperation>(this.GetExprComp<decimal?>);
            AddProcStruct<DoubleSearchParameters, double?, CompOperation>(this.GetExprComp<double?>);
            AddProcStruct<GuidSearchParameters, Guid?, EqualityOperation>(this.GetExprEq<Guid?>);
            AddProcStruct<Int16SearchParameters, short?, CompOperation>(this.GetExprComp<short?>);
            AddProcStruct<Int32SearchParameters, int?, CompOperation>(this.GetExprComp<int?>);
            AddProcStruct<Int64SearchParameters, long?, CompOperation>(this.GetExprComp<long?>);
            AddProcStruct<SingleSearchParameters, float?, CompOperation>(this.GetExprComp<float?>);
            AddProcClass<StringSearchParameters, string, StringOperation>(this.GetExprString);
            AddProcStruct<TimeSpanSearchParameters, TimeSpan?, CompOperation>(this.GetExprComp<TimeSpan?>);

            foreach (var prop in props)
            {
                var parms = prop.GetMethod.Invoke(parameters, new object[0]);
                processor[prop.PropertyType](prop, parms);
            }
        }

        private void ProcessStruct<T, TOp>(
            IWhereClauseBuilder<TObject> rs,
            PropertyInfo prop,
            FieldSearchParameters<T, TOp> parameters,
            Func<TOp, Expression<Func<T, T, bool>>> exprLookup)
        {
            if (parameters == null)
            {
                return;
            }

            var typeObj = typeof(TObject);
            var parameter = Expression.Parameter(typeObj, "rs");

            var propertyInfo = typeObj.GetInheritedProperty(prop.Name);

            var propCall = Expression.Convert(Expression.Property(
                                                                  (propertyInfo.DeclaringType == typeObj
                                                                       ? (Expression) parameter
                                                                       : Expression.Convert(parameter, propertyInfo.DeclaringType)),
                                                                  prop.Name), typeof(T));
            var labmda = Expression.Lambda<Func<TObject, T>>(propCall, parameter);

            var method = typeof(IWhereClauseBuilder<TObject>).GetMethod(
                                                                        nameof(IWhereClauseBuilder<TObject>.Field),
                                                                        types: new Type[]
                                                                               {
                                                                                   typeof(Expression<Func<TObject, T>>),
                                                                                   typeof(T),
                                                                                   typeof(Expression<Func<T, T, bool>>)
                                                                               });

            var expr = exprLookup(parameters.Operation);
            method.Invoke(rs, new object[] {labmda, parameters.Value, expr});
        }

        private void ProcessClass<T, TOp>(
            IWhereClauseBuilder<TObject> rs,
            PropertyInfo prop,
            FieldSearchParameters<T, TOp> parameters,
            Func<TOp, Expression<Func<T, T, bool>>> exprLookup)
            where T : class
        {
            if (parameters == null)
            {
                return;
            }

            var typeObj = typeof(TObject);
            var parameter = Expression.Parameter(typeObj, "rs");

            var propInfo = typeObj.GetInheritedProperty(prop.Name);

            var propCall = Expression.Property(
                                               propInfo.DeclaringType == typeObj
                                                   ? (Expression) parameter
                                                   : Expression.Convert(parameter, propInfo.DeclaringType),
                                               prop.Name);
            var labmda = Expression.Lambda<Func<TObject, string>>(propCall, parameter);

            var method = typeof(IWhereClauseBuilder<TObject>).GetMethod(
                                                                        nameof(IWhereClauseBuilder<TObject>.Field),
                                                                        types: new Type[]
                                                                               {
                                                                                   typeof(Expression<Func<TObject, T>>),
                                                                                   typeof(T),
                                                                                   typeof(Expression<Func<T, T, bool>>)
                                                                               });

            var expr = exprLookup(parameters.Operation);
            method.Invoke(rs, new object[] {labmda, parameters.Value, expr});
        }

        private Expression<Func<T, T, bool>> GetExprComp<T>(CompOperation op)
        {
            switch (op)
            {
                case CompOperation.Equal:
                    return Expressions.Equal<T>();

                case CompOperation.NotEqual:
                    return Expressions.NotEqual<T>();
                case CompOperation.GreaterThan:
                    return Expressions.GreaterThan<T>();
                case CompOperation.LessThan:
                    return Expressions.LessThan<T>();
                case CompOperation.GreaterThanOrEqual:
                    return Expressions.GreaterThanOrEqual<T>();
                case CompOperation.LessThanOrEqual:
                    return Expressions.LessThanOrEqual<T>();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private Expression<Func<string, string, bool>> GetExprString(StringOperation op)
        {
            switch (op)
            {
                case StringOperation.Equal:
                    return Expressions.Equal<string>();

                case StringOperation.NotEqual:
                    return Expressions.NotEqual<string>();

                case StringOperation.Like:
                    return Operations.Like;
            }

            throw new ArgumentOutOfRangeException();
        }

        private Expression<Func<T, T, bool>> GetExprEq<T>(EqualityOperation op)
        {
            switch (op)
            {
                case EqualityOperation.Equal:
                    return Expressions.Equal<T>();
                case EqualityOperation.NotEqual:
                    return Expressions.NotEqual<T>();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}