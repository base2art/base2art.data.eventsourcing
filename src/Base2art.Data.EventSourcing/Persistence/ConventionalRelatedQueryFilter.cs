namespace Base2art.Data.EventSourcing.Persistence
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using DataStorage.DataManipulation.Builders;
    using Tables;

    public class ConventionalRelatedQueryFilter<TParameters, TObject, TFirst> : IRelatedQueryFilter<TParameters, TObject, TFirst>
        where TObject : ITableRow, es_agg_published
        where TFirst : ITableRow, es_agg_published
    {
        private readonly Expression<Func<TObject, Guid>> firstLookup;

        public ConventionalRelatedQueryFilter(Expression<Func<TObject, Guid>> firstLookup)
        {
            this.firstLookup = firstLookup;
        }

        public void Where1(IWhereClauseBuilder<TObject> rs, TParameters parameters)
        {
            this.WhereAs(rs, parameters, 1);
        }

        public void Where2(IWhereClauseBuilder<TFirst> rs, TParameters parameters)
        {
            this.WhereAs(rs, parameters, 2);
        }

        protected void WhereAs<T>(IWhereClauseBuilder<T> rs, TParameters parameters, int number)
            where T : ITableRow, es_agg_published
        {
            if (parameters == null)
            {
                return;
            }

            string name = "";
            if (number > 1)
            {
                name = this.GetNameByLookupNumber(number);
            }

            var objType = typeof(T);
            var parmsType = parameters.GetType();
            var properties = parmsType.GetProperties();

            var typeMatches = properties.Where(x => string.Equals(
                                                                  x.PropertyType.Name.Replace("_search_data", "_published"),
                                                                  objType.Name,
                                                                  StringComparison.OrdinalIgnoreCase));

            var parmsProp = string.IsNullOrWhiteSpace(name)
                                ? typeMatches.SingleOrDefault()
                                : typeMatches.SingleOrDefault(x => x.Name == name);

            if (parmsProp == null)
            {
                throw new ArgumentOutOfRangeException("Property Not Found");
            }

            Type objSearchType = parmsProp.PropertyType;

            var instanceType = typeof(ConventionalQueryFilter<,>).MakeGenericType(objSearchType, objType);
            var instance = Activator.CreateInstance(instanceType);
            var filterMethod = instanceType.GetMethod("Filter");

            var value = parmsProp.GetMethod.Invoke(parameters, new object[0]);
            if (value == null)
            {
                return;
            }

            filterMethod.Invoke(instance, new[]
                                          {
                                              rs, value
                                          });
        }

        protected virtual string GetNameByLookupNumber(int number)
        {
            if (number == 2)
            {
                return this.GetName(this.firstLookup);
            }

            throw new ArgumentOutOfRangeException(nameof(number));
        }

        protected string GetName(Expression<Func<TObject, Guid>> expr)
        {
            var memberExpr = expr.Body as MemberExpression;

            var member = memberExpr?.Member?.Name ?? string.Empty;
            if (member.EndsWith("_id"))
            {
                return member.Substring(0, member.Length - 3);
            }

            return member;
        }
    }

    public class ConventionalRelatedQueryFilter<TParameters, TObject, TFirst, TSecond>
        : ConventionalRelatedQueryFilter<TParameters, TObject, TFirst>,
          IRelatedQueryFilter<TParameters, TObject, TFirst, TSecond>
        where TObject : ITableRow, es_agg_published
        where TFirst : ITableRow, es_agg_published
        where TSecond : ITableRow, es_agg_published
    {
        private readonly Expression<Func<TObject, Guid>> secondLookup;

        public ConventionalRelatedQueryFilter(Expression<Func<TObject, Guid>> firstLookup, Expression<Func<TObject, Guid>> secondLookup)
            : base(firstLookup)
        {
            this.secondLookup = secondLookup;
        }

        public void Where3(IWhereClauseBuilder<TSecond> rs, TParameters parameters)
        {
            this.WhereAs(rs, parameters, 3);
        }

        protected override string GetNameByLookupNumber(int number)
        {
            if (number == 3)
            {
                return this.GetName(this.secondLookup);
            }

            return base.GetNameByLookupNumber(number);
        }
    }

    public class ConventionalRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird>
        : ConventionalRelatedQueryFilter<TParameters, TObject, TFirst, TSecond>,
          IRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird>
        where TObject : ITableRow, es_agg_published
        where TFirst : ITableRow, es_agg_published
        where TSecond : ITableRow, es_agg_published
        where TThird : ITableRow, es_agg_published
    {
        private readonly Expression<Func<TObject, Guid>> thirdLookup;

        public ConventionalRelatedQueryFilter(
            Expression<Func<TObject, Guid>> firstLookup,
            Expression<Func<TObject, Guid>> secondLookup,
            Expression<Func<TObject, Guid>> thirdLookup)
            : base(firstLookup, secondLookup)
        {
            this.thirdLookup = thirdLookup;
        }

        public void Where4(IWhereClauseBuilder<TThird> rs, TParameters parameters)
        {
            this.WhereAs(rs, parameters, 4);
        }

        protected override string GetNameByLookupNumber(int number)
        {
            if (number == 4)
            {
                return this.GetName(this.thirdLookup);
            }

            return base.GetNameByLookupNumber(number);
        }
    }

    public class ConventionalRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird, TFourth>
        : ConventionalRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird>,
          IRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird, TFourth>
        where TObject : ITableRow, es_agg_published
        where TFirst : ITableRow, es_agg_published
        where TSecond : ITableRow, es_agg_published
        where TThird : ITableRow, es_agg_published
        where TFourth : ITableRow, es_agg_published
    {
        private readonly Expression<Func<TObject, Guid>> fourthLookup;

        public ConventionalRelatedQueryFilter(
            Expression<Func<TObject, Guid>> firstLookup,
            Expression<Func<TObject, Guid>> secondLookup,
            Expression<Func<TObject, Guid>> thirdLookup,
            Expression<Func<TObject, Guid>> fourthLookup)
            : base(firstLookup, secondLookup, thirdLookup)
        {
            this.fourthLookup = fourthLookup;
        }

        public void Where5(IWhereClauseBuilder<TFourth> rs, TParameters parameters)
        {
            this.WhereAs(rs, parameters, 5);
        }

        protected override string GetNameByLookupNumber(int number)
        {
            if (number == 5)
            {
                return this.GetName(this.fourthLookup);
            }

            return base.GetNameByLookupNumber(number);
        }
    }

    public class ConventionalRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird, TFourth, TFifth>
        : ConventionalRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird, TFourth>,
          IRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird, TFourth, TFifth>
        where TObject : ITableRow, es_agg_published
        where TFirst : ITableRow, es_agg_published
        where TSecond : ITableRow, es_agg_published
        where TThird : ITableRow, es_agg_published
        where TFourth : ITableRow, es_agg_published
        where TFifth : ITableRow, es_agg_published
    {
        private readonly Expression<Func<TObject, Guid>> fifthLookup;

        public ConventionalRelatedQueryFilter(
            Expression<Func<TObject, Guid>> firstLookup,
            Expression<Func<TObject, Guid>> secondLookup,
            Expression<Func<TObject, Guid>> thirdLookup,
            Expression<Func<TObject, Guid>> fourthLookup,
            Expression<Func<TObject, Guid>> fifthLookup)
            : base(firstLookup, secondLookup, thirdLookup, fourthLookup)
        {
            this.fifthLookup = fifthLookup;
        }

        public void Where6(IWhereClauseBuilder<TFifth> rs, TParameters parameters)
        {
            this.WhereAs(rs, parameters, 6);
        }

        protected override string GetNameByLookupNumber(int number)
        {
            if (number == 6)
            {
                return this.GetName(this.fifthLookup);
            }

            return base.GetNameByLookupNumber(number);
        }
    }

    public class ConventionalRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird, TFourth, TFifth, TSixth>
        : ConventionalRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird, TFourth, TFifth>,
          IRelatedQueryFilter<TParameters, TObject, TFirst, TSecond, TThird, TFourth, TFifth, TSixth>
        where TObject : ITableRow, es_agg_published
        where TFirst : ITableRow, es_agg_published
        where TSecond : ITableRow, es_agg_published
        where TThird : ITableRow, es_agg_published
        where TFourth : ITableRow, es_agg_published
        where TFifth : ITableRow, es_agg_published
        where TSixth : ITableRow, es_agg_published
    {
        private readonly Expression<Func<TObject, Guid>> sixthLookup;

        public ConventionalRelatedQueryFilter(
            Expression<Func<TObject, Guid>> firstLookup,
            Expression<Func<TObject, Guid>> secondLookup,
            Expression<Func<TObject, Guid>> thirdLookup,
            Expression<Func<TObject, Guid>> fourthLookup,
            Expression<Func<TObject, Guid>> fifthLookup,
            Expression<Func<TObject, Guid>> sixthLookup)
            : base(firstLookup, secondLookup, thirdLookup, fourthLookup, fifthLookup)
        {
            this.sixthLookup = sixthLookup;
        }

        public void Where7(IWhereClauseBuilder<TSixth> rs, TParameters parameters)
        {
            this.WhereAs(rs, parameters, 7);
        }

        protected override string GetNameByLookupNumber(int number)
        {
            if (number == 7)
            {
                return this.GetName(this.sixthLookup);
            }

            return base.GetNameByLookupNumber(number);
        }
    }
}