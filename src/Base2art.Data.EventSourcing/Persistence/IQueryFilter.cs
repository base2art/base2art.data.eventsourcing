namespace Base2art.Data.EventSourcing.Persistence
{
    using DataStorage.DataManipulation.Builders;

    public interface IQueryFilter<in TSearchParameters, TItem>
    {
        void Filter(IWhereClauseBuilder<TItem> rs, TSearchParameters parameters);
    }
}