// ReSharper disable InconsistentNaming

namespace Base2art.Data.EventSourcing.Persistence.Tables
{
    using System;

    public interface es_agg_published
    {
        Guid object_id { get; }
        Guid? version_id { get; }
        int? version_number { get; }

        Guid? created_from { get; }
        Guid created_by { get; }
        DateTime created_at { get; }
    }
}