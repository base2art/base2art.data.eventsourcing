// ReSharper disable InconsistentNaming

namespace Base2art.Data.EventSourcing.Persistence.Tables
{
    using System;

    public interface es_object
    {
        Guid object_id { get; }
    }
}