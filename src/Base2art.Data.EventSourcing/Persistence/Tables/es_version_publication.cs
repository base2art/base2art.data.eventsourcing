// ReSharper disable InconsistentNaming

namespace Base2art.Data.EventSourcing.Persistence.Tables
{
    using System;

    public interface es_version_publication
    {
        Guid id { get; }

        Guid object_id { get; }

        Guid? version_id { get; }

        Guid? version_created_from { get; }
        
        int? version_number { get; }

        Guid? previously_publication_id { get; }

        Guid created_by { get; }

        DateTime created_at { get; }
    }
}