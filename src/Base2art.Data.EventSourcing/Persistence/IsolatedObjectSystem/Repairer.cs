namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;
    using DataStorage;
    using DataStorage.DataManipulation.Builders;
    using Tables;

    public class Repairer<T, TPublished, TPublicationTable, TVersionTable> : IRepairer
        where T : ITableRow
        where TPublished : T, es_agg_published
        where TPublicationTable : class, es_version_publication
        where TVersionTable : class, es_version
    {
        private readonly IDataStore store;
        private readonly IDataBinder<T> dataBinder;

        public Repairer(IDataStore store, IDataBinder<T> dataBinder)
        {
            this.store = store;
            this.dataBinder = dataBinder;
        }

        public async Task RepairPublished(Guid id)
        {
            var pubContainer = await this.store.GetLatestPublicationWithVersion<TPublicationTable, TVersionTable>(id);

            if (pubContainer == null)
            {
                // DELETE
                // MARKER: DELETE
                await this.store
                          .Delete<TPublished>()
                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
                          .Execute();
                return;
            }

            var pub = pubContainer.Item1;
            var pub2 = pubContainer.Item2;

            var aggs = await this.store
                                 .Select<TPublished>()
                                 .WithNoLock()
                                 .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
                                 .Execute();

            var aggsCount = Enumerable.Count(aggs);
            if (aggsCount > 1)
            {
                // DELETE
                // MARKER: DELETE
                await this.store
                          .Delete<TPublished>()
                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
                          .Execute();
                aggsCount = 0;
            }

            var result = await this.store.SelectSingle<T>()
                                   .Where(rs => rs.Field(x => x.id, pub.version_id, (x, y) => x == y))
                                   .Execute();

            if (aggsCount == 1)
            {
                // update
                await this.store.Update<TPublished>()
                          .Set(rs =>
                          {
                              rs.Field(x => x.version_id, pub.version_id)
                                .Field(x => x.created_by, pub2.created_by)
                                .Field(x => x.created_from, pub2.created_from)
                                .Field(x => x.id, id)
                                .Field(x => x.created_at, pub2.created_at)
                                .Field(x => x.version_number, pub2.version_number);

                              this.Binder.Bind(new WrapperRs(rs), result);
                          })
                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
                          .Execute();
            }
            else
            {
                await this.store.Insert<TPublished>()
                          .Record(rs =>
                          {
                              rs.Field(x => x.object_id, id)
                                .Field(x => x.version_id, pub2.version_id)
                                .Field(x => x.created_by, pub2.created_by)
                                .Field(x => x.created_from, pub2.created_from)
                                .Field(x => x.id, id)
                                .Field(x => x.created_at, pub2.created_at)
                                .Field(x => x.version_number, pub2.version_number);
                              this.Binder.Bind(new WrapperRs(rs), result);
                          })
                          .Execute();
            }
        }

        private class WrapperRs : ISetListBuilder<T>
        {
            private readonly ISetListBuilder<TPublished> setListBuilderImplementation;
//                private ISetListBuilder<T> ;

            public WrapperRs(ISetListBuilder<TPublished> rs)
            {
                this.setListBuilderImplementation = rs;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, string>> caller, string data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, byte[]>> caller, byte[] data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, Guid?>> caller, Guid? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, long?>> caller, long? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, int?>> caller, int? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, DateTime?>> caller, DateTime? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller, TimeSpan? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, short?>> caller, short? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, XmlDocument>> caller, XmlDocument data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, XElement>> caller, XElement data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, decimal?>> caller, decimal? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, float?>> caller, float? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, double?>> caller, double? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, bool?>> caller, bool? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller, Dictionary<string, object> data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            private Expression<Func<TPublished, T1>> Wrap<T1>(Expression<Func<T, T1>> caller)
            {
                var memberExpression = caller.Body as MemberExpression;
                UnaryExpression convert = null;
                if (memberExpression == null)
                {
                    var unaryExpression = caller.Body as UnaryExpression;
                    if (unaryExpression != null)
                    {
                        memberExpression = unaryExpression.Operand as MemberExpression;
                        convert = unaryExpression;
                    }
                }

                var memberInfo = memberExpression.Member;

                var parameter = Expression.Parameter(typeof(TPublished), "rs");
                var propertyInfo = memberInfo as PropertyInfo;
                Expression prop = Expression.Property(parameter, propertyInfo);
                var lambda = Expression.Lambda<Func<TPublished, T1>>(
                                                                     (convert == null
                                                                          ? prop
                                                                          : Expression.Convert(prop, this.MakeNullable(propertyInfo.PropertyType))),
                                                                     parameter);

                return lambda;
            }

            private Type MakeNullable(Type type)
            {
                return typeof(Nullable<>).MakeGenericType(type);
            }
        }

        public IDataBinder<T> Binder => this.dataBinder ?? new DynamicBinder<T>();

        public async Task CreateTables(IDbms dbms)
        {
            await dbms.CreateTable<TPublished>().IfNotExists().Execute();
        }
    }

    public class Repairer<T, TPublished, TPublicationTable> : IRepairer
        where T : ITableRow
        where TPublished : T, es_agg_published
        where TPublicationTable : class, es_version_publication
    {
        private readonly IDataStore store;
        private readonly IDataBinder<T> dataBinder;

        public Repairer(IDataStore store, IDataBinder<T> dataBinder)
        {
            this.store = store;
            this.dataBinder = dataBinder;
        }

        public async Task RepairPublished(Guid id)
        {
            var pubContainer = await this.store.GetLatestPublication<TPublicationTable>(id);

            if (pubContainer == null)
            {
                // DELETE
                // MARKER: DELETE
                await this.store
                          .Delete<TPublished>()
                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
                          .Execute();
                return;
            }

            var pub = pubContainer;

            var aggs = await this.store
                                 .Select<TPublished>()
                                 .WithNoLock()
                                 .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
                                 .Execute();

            var aggsCount = Enumerable.Count(aggs);
            if (aggsCount > 1)
            {
                // DELETE
                // MARKER: DELETE
                await this.store
                          .Delete<TPublished>()
                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
                          .Execute();
                aggsCount = 0;
            }

            var result = await this.store.SelectSingle<T>()
                                   .Where(rs => rs.Field(x => x.id, pub.version_id, (x, y) => x == y))
                                   .Execute();
//
            if (aggsCount == 1)
            {
                // update
                await this.store.Update<TPublished>()
                          .Set(rs =>
                          {
                              rs.Field(x => x.version_id, pub.version_id)
                                .Field(x => x.created_by, pub.created_by)
                                .Field(x => x.created_from, pub.version_created_from)
                                .Field(x => x.id, id)
                                .Field(x => x.created_at, pub.created_at)
                                .Field(x => x.version_number, pub.version_number);

                              this.Binder.Bind(new WrapperRs(rs), result);
                          })
                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
                          .Execute();
            }
            else
            {
                var type = typeof(TPublished).ToString();
                Console.WriteLine(type);
                await this.store.Insert<TPublished>()
                          .Record(rs =>
                          {
                              rs.Field(x => x.object_id, id)
                                .Field(x => x.version_id, pub.version_id)
                                .Field(x => x.created_by, pub.created_by)
                                .Field(x => x.created_from, pub.version_created_from)
                                .Field(x => x.id, id)
                                .Field(x => x.created_at, pub.created_at)
                                .Field(x => x.version_number, pub.version_number);
                              this.Binder.Bind(new WrapperRs(rs), result);
                          })
                          .Execute();
            }
        }

        private class WrapperRs : ISetListBuilder<T>
        {
            private readonly ISetListBuilder<TPublished> setListBuilderImplementation;
//                private ISetListBuilder<T> ;

            public WrapperRs(ISetListBuilder<TPublished> rs)
            {
                this.setListBuilderImplementation = rs;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, string>> caller, string data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, byte[]>> caller, byte[] data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, Guid?>> caller, Guid? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, long?>> caller, long? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, int?>> caller, int? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, DateTime?>> caller, DateTime? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller, TimeSpan? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, short?>> caller, short? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, XmlDocument>> caller, XmlDocument data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, XElement>> caller, XElement data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, decimal?>> caller, decimal? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, float?>> caller, float? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, double?>> caller, double? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, bool?>> caller, bool? data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            public ISetListBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller, Dictionary<string, object> data)
            {
                this.setListBuilderImplementation.Field(this.Wrap(caller), data);
                return this;
            }

            private Expression<Func<TPublished, T1>> Wrap<T1>(Expression<Func<T, T1>> caller)
            {
                var memberExpression = caller.Body as MemberExpression;
                UnaryExpression convert = null;
                if (memberExpression == null)
                {
                    var unaryExpression = caller.Body as UnaryExpression;
                    if (unaryExpression != null)
                    {
                        memberExpression = unaryExpression.Operand as MemberExpression;
                        convert = unaryExpression;
                    }
                }

                var memberInfo = memberExpression.Member;

                var parameter = Expression.Parameter(typeof(TPublished), "rs");
                var propertyInfo = memberInfo as PropertyInfo;
                Expression prop = Expression.Property(parameter, propertyInfo);
                var lambda = Expression.Lambda<Func<TPublished, T1>>(
                                                                     (convert == null
                                                                          ? prop
                                                                          : Expression.Convert(prop, this.MakeNullable(propertyInfo.PropertyType))),
                                                                     parameter);

                return lambda;
            }

            private Type MakeNullable(Type type)
            {
                return typeof(Nullable<>).MakeGenericType(type);
            }
        }

        public IDataBinder<T> Binder => this.dataBinder ?? new DynamicBinder<T>();

        public async Task CreateTables(IDbms dbms)
        {
            await dbms.CreateTable<TPublished>().IfNotExists().Execute();
        }
    }
}