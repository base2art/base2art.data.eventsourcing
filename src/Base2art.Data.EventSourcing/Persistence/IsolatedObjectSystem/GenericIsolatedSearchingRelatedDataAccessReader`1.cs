namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using DataStorage;
    using DataStorage.DataManipulation;
    using DataStorage.DataManipulation.Builders;
    using Modeling;

    public class GenericIsolatedSearchingRelatedDataAccessReader<TParameters, TJoinAgg, TFirstAgg>
        : GenericIsolatedSearchingRelatedDataAccessReaderBase<TParameters, TJoinAgg, Tuple<TJoinAgg, TFirstAgg>>,
          IDataAccessSearcher<TParameters, Tuple<TJoinAgg, TFirstAgg>>,
          IRelatedDataAccessSearcher<TParameters, TJoinAgg, TFirstAgg>
        where TJoinAgg : ITableRow, Tables.es_agg_published
        where TFirstAgg : ITableRow, Tables.es_agg_published
    {
        private IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg> ManyManyQueryFilter { get; }

        private readonly Expression<Func<TJoinAgg, Guid>> firstLookup;

        public GenericIsolatedSearchingRelatedDataAccessReader(
            IDataStore store,
            Expression<Func<TJoinAgg, Guid>> firstLookup,
            IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg> searcher)
            : base(store)
        {
            this.firstLookup = firstLookup;
            this.ManyManyQueryFilter = searcher ?? new ConventionalRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg>(this.firstLookup);
        }

        protected void Parameters1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where1(rs, parameters);
        }

        protected void Parameters2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where2(rs, parameters);
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg>>>> FindPublishedItems(TParameters parameters)
        {
            return this.SearchFor(parameters, x => x);
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg>>>> FindPublishedItems(TParameters parameters, IndexPagination pagination)
        {
            return this.SearchFor(parameters, x => x.Limit(pagination.PageSize).Offset(pagination.PageIndex * pagination.PageSize));
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg>>>> FindPublishedItems(TParameters parameters, OffsetPagination pagination)
        {
            return this.SearchFor(parameters, x => x);
        }

        private async Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg>>>> SearchFor(
            TParameters parameters,
            Func<IQuerySelect<TJoinAgg>, IQuerySelect<TJoinAgg>> lookup)
        {
            var rootParameterExpression = this.RootParameterExpression();
            var result = this.GetAggregate<TFirstAgg>(0, rootParameterExpression);
            var lambda1 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, bool>>(result.Item1, rootParameterExpression, result.Item2);
            var items = await lookup(this.Store
                                         .Select<TJoinAgg>())
                              .WithNoLock()
                              .Join<TFirstAgg>(lambda1)
                              .Where1(rs => this.Parameters1(rs, parameters))
                              .Where2(rs => this.Parameters2(rs, parameters))
                              .Execute();

            return items.Select(x => new ObjectEntityRow<Tuple<TJoinAgg, TFirstAgg>>(x, y => y.Item1));
        }

        protected override Expression<Func<TJoinAgg, Guid>> Lookup(int typeIndex)
        {
            if (typeIndex == 0)
            {
                return this.firstLookup;
            }

            throw new ArgumentOutOfRangeException(nameof(typeIndex));
        }
    }
}