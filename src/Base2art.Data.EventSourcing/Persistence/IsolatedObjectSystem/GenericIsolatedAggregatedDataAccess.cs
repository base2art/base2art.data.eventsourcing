//namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
//{
//    public class GenericIsolatedAggregatedDataAccess
//    {
//        
//
//        private async Task RepairAsync(Guid id)
//        {
//            await this.RepairLatest(id);
//            await this.RepairPublished(id);
//        }
//
//        private async Task RepairLatest(Guid id)
//        {
//            var results = await this.Store.SelectSingle<TObjectVersionTable>()
//                                    .LeftJoin<TObjectVersionTable>((x, y) => x.created_from == y.version_id)
//                                    .Where1(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
//                                    .Execute();
//
//            if ((results?.Item1?.created_at).GetValueOrDefault() == default)
//            {
//                await this.Store
//                          .Delete<TAggLatestTable>()
//                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
//                          .Execute();
//
//                return;
//            }
//
//            var aggs = await this.Store
//                                 .Select<TAggLatestTable>()
//                                 .WithNoLock()
//                                 .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
//                                 .Execute();
//
//            var aggsCount = aggs.Count();
//            if (aggsCount > 1)
//            {
//                // DELETE
//                await this.Store
//                          .Delete<TAggLatestTable>()
//                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
//                          .Execute();
//                aggsCount = 0;
//            }
//
//            if (aggsCount == 1)
//            {
//                // update
//                await this.Store.Update<TAggLatestTable>()
//                          .Set(rs => rs.Field(x => x.version_id, results.Item1.version_id)
//                                       .Field(x => x.data_id, results.Item1.version_id))
//                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
//                          .Execute();
//            }
//            else
//            {
//                await this.Store.Insert<TAggLatestTable>()
//                          .Record(rs => rs.Field(x => x.object_id, id)
//                                          .Field(x => x.version_id, results.Item1.version_id)
//                                          .Field(x => x.data_id, results.Item1.version_id))
//                          .Execute();
//            }
//        }
//
//        private async Task RepairPublished(Guid id)
//        {
//            var pub = await this.Store.GetLatestPublication<TPublicationTable>(id);
//
//            if (pub == null)
//            {
//                // DELETE
//                await this.Store
//                          .Delete<TAggPublishedTable>()
//                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
//                          .Execute();
//                return;
//            }
//
//            var aggs = await this.Store
//                                 .Select<TAggPublishedTable>()
//                                 .WithNoLock()
//                                 .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
//                                 .Execute();
//
//            var aggsCount = aggs.Count();
//            if (aggsCount > 1)
//            {
//                // DELETE
//                await this.Store
//                          .Delete<TAggPublishedTable>()
//                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
//                          .Execute();
//                aggsCount = 0;
//            }
//
//            if (aggsCount == 1)
//            {
//                // update
//                await this.Store.Update<TAggPublishedTable>()
//                          .Set(rs => rs.Field(x => x.version_id, pub.version_id)
//                                       .Field(x => x.data_id, pub.version_id))
//                          .Where(rs => rs.Field(x => x.object_id, id, (x, y) => x == y))
//                          .Execute();
//            }
//            else
//            {
//                await this.Store.Insert<TAggPublishedTable>()
//                          .Record(rs => rs.Field(x => x.object_id, id)
//                                          .Field(x => x.version_id, pub.version_id)
//                                          .Field(x => x.data_id, pub.version_id))
//                          .Execute();
//            }
//        }
//    }
//}