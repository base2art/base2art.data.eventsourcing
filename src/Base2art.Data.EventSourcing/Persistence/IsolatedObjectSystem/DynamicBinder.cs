namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using DataStorage.DataManipulation.Builders;

    public class DynamicBinder<T> : IDataBinder<T>
    {
        public ISetListBuilder<T> Bind(ISetListBuilder<T> field, T data)
        {
            var type = typeof(T);
            var props = type.GetProperties();

            foreach (var prop in props)
            {
                var pt = prop.PropertyType;
                if (prop.Name == nameof(ITableRow.id) && pt == typeof(Guid))
                {
                    // SKIP
                }
                else
                {
                    ParameterExpression argParam = Expression.Parameter(typeof(T), "rs");
//                    Expression nameProperty1 = pt.IsValueType
//                                                   ? Expression.Property(argParam,  typeof(Nullable<>).MakeGenericType(pt), prop.Name)
//                                                   : Expression.Property(argParam, prop);
                    Expression nameProperty1 = Expression.Property(argParam, prop);

                    if (pt.GetTypeInfo().IsValueType && !(IsNullable(pt)))
                    {
                        nameProperty1 = Expression.Convert(nameProperty1, typeof(Nullable<>).MakeGenericType(pt));
                    }

//                    var val1 = Expression.Constant("Modules");

//                    Expression e1 = Expression.Equal(nameProperty, val1);

                    var datax = prop.GetMethod.Invoke(data, null);
                    field = Call(pt, field, nameProperty1, argParam, datax);
                }
            }

            return field;
        }

        private static bool IsNullable(Type type) => Nullable.GetUnderlyingType(type) != null;

        private static ISetListBuilder<T> Call(Type dataType, ISetListBuilder<T> field, Expression nameProperty, ParameterExpression argParam,
                                               object datax)
        {
            MethodInfo method = typeof(DynamicBinder<T>).GetMethod(nameof(CallGeneric), BindingFlags.Static | BindingFlags.NonPublic);

            if (dataType.GetTypeInfo().IsValueType && !IsNullable(dataType))
            {
                dataType = typeof(Nullable<>).MakeGenericType(dataType);
            }

            MethodInfo generic = method.MakeGenericMethod(dataType);
            return (ISetListBuilder<T>) generic.Invoke(null, new[]
                                                             {
                                                                 field,
                                                                 nameProperty,
                                                                 argParam,
                                                                 datax
                                                             });
        }

        private static ISetListBuilder<T> CallGeneric<TInner>(ISetListBuilder<T> field, Expression nameProperty, ParameterExpression argParam,
                                                              TInner datax)
        {
            var lambda = Expression.Lambda<Func<T, TInner>>(nameProperty, argParam);

            var type = typeof(ISetListBuilder<T>);
            var method = type.GetMethod(
                                        nameof(ISetListBuilder<T>.Field),
                                        new Type[] {lambda.GetType(), typeof(TInner)});

//            if (method == null)
//            {
//                // lambda.GetType()
//                var nullableType = typeof(Nullable<>).MakeGenericType(typeof(TInner));
//                var funcType = typeof(Func<,>).MakeGenericType(new[] {typeof(T), nullableType});
//                var targetType = typeof(Expression<>).MakeGenericType(funcType);
//                method = type.GetMethod(
//                                        nameof(ISetListBuilder<T>.Field),
//                                        new Type[] {targetType, nullableType});
//            }

            var invoke = method.Invoke(field, new object[] {lambda, datax});
            return (ISetListBuilder<T>) invoke;
        }
    }
}