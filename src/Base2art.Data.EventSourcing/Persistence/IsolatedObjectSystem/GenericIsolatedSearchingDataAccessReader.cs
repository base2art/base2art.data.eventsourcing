namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using DataStorage;
    using DataStorage.DataManipulation;
    using DataStorage.DataManipulation.Builders;
    using Modeling;

    public class GenericIsolatedSearchingDataAccessReader<TParameters, TJoinAgg>
        : IDataAccessSearcher<TParameters, TJoinAgg>
        where TJoinAgg : ITableRow, Tables.es_agg_published
    {
        protected IDataStore Store { get; }

        protected IQueryFilter<TParameters, TJoinAgg> QueryFilter { get; }

        public GenericIsolatedSearchingDataAccessReader(
            IDataStore store,
            IQueryFilter<TParameters, TJoinAgg> searcher)
        {
            this.QueryFilter = searcher ?? new ConventionalQueryFilter<TParameters, TJoinAgg>();
            this.Store = store;
        }

        public async Task<IEnumerable<IObjectEntityRow<TJoinAgg>>> FindPublishedItems(TParameters parameters)
        {
            return await this.SearchFor(parameters, x => x);
        }

        public Task<IEnumerable<IObjectEntityRow<TJoinAgg>>> FindPublishedItems(TParameters parameters, IndexPagination pagination)
        {
            return this.SearchFor(parameters, x => x.Limit(pagination.PageSize).Offset(pagination.PageIndex * pagination.PageSize));
        }

        public Task<IEnumerable<IObjectEntityRow<TJoinAgg>>> FindPublishedItems(TParameters parameters, OffsetPagination pagination)
        {
            return this.SearchFor(parameters, x => x);
        }

        private async Task<IEnumerable<IObjectEntityRow<TJoinAgg>>> SearchFor(
            TParameters parameters,
            Func<IQuerySelect<TJoinAgg>, IQuerySelect<TJoinAgg>> limitOffest)
        {
            var items = await limitOffest(this.Store
                                              .Select<TJoinAgg>())
                              .WithNoLock()
                              .Where(rs => this.Parameters1(rs, parameters))
                              .Execute();

            return items.Select(x => new ObjectEntityRow(x));
        }

        protected virtual void Parameters1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters)
        {
            this.QueryFilter.Filter(rs, parameters);
        }

        public class ObjectEntityRow : IObjectEntityRow<TJoinAgg>
        {
            private readonly TJoinAgg tuple;

            public ObjectEntityRow(TJoinAgg tuple)
            {
                this.tuple = tuple;
            }

            public Guid Id => this.tuple.object_id;
            public IVersionData RepresentedVersion => new VersionData(this.tuple);
            public TJoinAgg Data => this.tuple;
        }

        private class VersionData : IVersionData
        {
            public VersionData(Tables.es_agg_published versionData)
            {
                this.VersionNumber = versionData.version_number.GetValueOrDefault();
                this.VersionId = versionData.version_id.GetValueOrDefault();
                this.CreatedFrom = versionData.created_from;
                this.CreatedBy = versionData.created_by;
                this.CreatedAt = versionData.created_at;
            }

            public int VersionNumber { get; }
            public Guid VersionId { get; }
            public Guid? CreatedFrom { get; }
            public Guid CreatedBy { get; }
            public DateTime CreatedAt { get; }
        }
    }
}