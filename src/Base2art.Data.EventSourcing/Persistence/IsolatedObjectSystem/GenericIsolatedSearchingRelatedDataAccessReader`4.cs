namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using DataStorage;
    using DataStorage.DataManipulation;
    using DataStorage.DataManipulation.Builders;
    using Modeling;

    public class GenericIsolatedSearchingRelatedDataAccessReader<TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>
        : GenericIsolatedSearchingRelatedDataAccessReaderBase<TParameters, TJoinAgg, Tuple<TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>>,
          IDataAccessSearcher<TParameters, Tuple<TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>>,
          IRelatedDataAccessSearcher<TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>
        where TJoinAgg : ITableRow, Tables.es_agg_published
        where TFirstAgg : ITableRow, Tables.es_agg_published
        where TSecondAgg : ITableRow, Tables.es_agg_published
        where TThirdAgg : ITableRow, Tables.es_agg_published
        where TFourth : ITableRow, Tables.es_agg_published
    {
        private readonly Expression<Func<TJoinAgg, Guid>> firstLookup;
        private readonly Expression<Func<TJoinAgg, Guid>> secondLookup;
        private readonly Expression<Func<TJoinAgg, Guid>> thirdLookup;
        private readonly Expression<Func<TJoinAgg, Guid>> fourthLookup;

        protected IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth> ManyManyQueryFilter { get; }

        public GenericIsolatedSearchingRelatedDataAccessReader(
            IDataStore store,
            Expression<Func<TJoinAgg, Guid>> firstLookup,
            Expression<Func<TJoinAgg, Guid>> secondLookup,
            Expression<Func<TJoinAgg, Guid>> thirdLookup,
            Expression<Func<TJoinAgg, Guid>> fourthLookup,
            IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth> searcher)
            : base(store)
        {
            this.firstLookup = firstLookup;
            this.secondLookup = secondLookup;
            this.thirdLookup = thirdLookup;
            this.fourthLookup = fourthLookup;
            this.ManyManyQueryFilter = searcher
                                       ?? new ConventionalRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>(
                                                                                                                                               this
                                                                                                                                                   .firstLookup,
                                                                                                                                               this
                                                                                                                                                   .secondLookup,
                                                                                                                                               this
                                                                                                                                                   .thirdLookup,
                                                                                                                                               this
                                                                                                                                                   .fourthLookup);
        }

        protected virtual void Parameters1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where1(rs, parameters);
        }

        protected virtual void Parameters2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where2(rs, parameters);
        }

        protected virtual void Parameters3(IWhereClauseBuilder<TSecondAgg> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where3(rs, parameters);
        }

        protected virtual void Parameters4(IWhereClauseBuilder<TThirdAgg> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where4(rs, parameters);
        }

        protected virtual void Parameters5(IWhereClauseBuilder<TFourth> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where5(rs, parameters);
        }

        protected override Expression<Func<TJoinAgg, Guid>> Lookup(int typeIndex)
        {
            if (typeIndex == 0)
            {
                return this.firstLookup;
            }

            if (typeIndex == 1)
            {
                return this.secondLookup;
            }

            if (typeIndex == 2)
            {
                return this.thirdLookup;
            }

            if (typeIndex == 3)
            {
                return this.fourthLookup;
            }

            throw new ArgumentOutOfRangeException();
        }

        public async Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>>>> FindPublishedItems(
            TParameters parameters)
        {
            return await this.SearchFor(parameters, x => x);
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>>>> FindPublishedItems(
            TParameters parameters, IndexPagination pagination)
        {
            return this.SearchFor(parameters, x => x.Limit(pagination.PageSize).Offset(pagination.PageIndex * pagination.PageSize));
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>>>> FindPublishedItems(
            TParameters parameters,
            OffsetPagination pagination)
        {
            return this.SearchFor(parameters, x => x);
        }

        private async Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>>>> SearchFor(
            TParameters parameters,
            Func<IQuerySelect<TJoinAgg>, IQuerySelect<TJoinAgg>> lookup)
        {
            var rootParameterExpression = this.RootParameterExpression();
            var result1 = this.GetAggregate<TFirstAgg>(0, rootParameterExpression);
            var result2 = this.GetAggregate<TSecondAgg>(1, rootParameterExpression);
            var result3 = this.GetAggregate<TThirdAgg>(2, rootParameterExpression);
            var result4 = this.GetAggregate<TFourth>(3, rootParameterExpression);

            var lambda1 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, bool>>(result1.Item1, rootParameterExpression, result1.Item2);

            var lambda2 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, TSecondAgg, bool>>(
                                                                                         result2.Item1,
                                                                                         rootParameterExpression,
                                                                                         result1.Item2,
                                                                                         result2.Item2);

            var lambda3 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, bool>>(
                                                                                                    result3.Item1,
                                                                                                    rootParameterExpression,
                                                                                                    result1.Item2,
                                                                                                    result2.Item2,
                                                                                                    result3.Item2);

            var lambda4 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth, bool>>(
                                                                                                             result4.Item1,
                                                                                                             rootParameterExpression,
                                                                                                             result1.Item2,
                                                                                                             result2.Item2,
                                                                                                             result3.Item2,
                                                                                                             result4.Item2);

            var items = await lookup(this.Store.Select<TJoinAgg>())
                              .WithNoLock()
                              .Join<TFirstAgg>(lambda1)
                              .Join<TSecondAgg>(lambda2)
                              .Join<TThirdAgg>(lambda3)
                              .Join<TFourth>(lambda4)
                              .Where1(rs => this.Parameters1(rs, parameters))
                              .Where2(rs => this.Parameters2(rs, parameters))
                              .Where3(rs => this.Parameters3(rs, parameters))
                              .Where4(rs => this.Parameters4(rs, parameters))
                              .Where5(rs => this.Parameters5(rs, parameters))
                              .Execute();

            return items.Select(x => new ObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg, TThirdAgg, TFourth>>(x, y => y.Item1));
        }
    }
}