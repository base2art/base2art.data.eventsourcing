//namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Linq.Expressions;
//    using System.Reflection;
//    using System.Threading.Tasks;
//    using DataStorage;
//    using DataStorage.DataManipulation.Builders;
//    using Modeling;
//
//    public class GenericIsolatedSearchingRelatedDataAccessReader<TParameters, TJoinAgg, TFirstAgg, TSecondAgg>
//        : GenericIsolatedSearchingRelatedDataAccessReaderBase<TParameters, TJoinAgg, Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>,
//          IDataAccessSearcher<TParameters, Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>,
//          IRelatedDataAccessSearcher<TParameters, TJoinAgg, TFirstAgg, TSecondAgg>
//        where TJoinAgg : ITableRow, Tables.es_agg_published
//        where TFirstAgg : ITableRow, Tables.es_agg_published
//        where TSecondAgg : ITableRow, Tables.es_agg_published
//    {
//        private readonly Expression<Func<TJoinAgg, Guid>> firstLookup;
//        private readonly Expression<Func<TJoinAgg, Guid>> secondLookup;
//
//        protected IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg> ManyManyQueryFilter { get; }
//
//        public GenericIsolatedSearchingRelatedDataAccessReader(
//            IDataStore store,
//            Expression<Func<TJoinAgg, Guid>> firstLookup,
//            Expression<Func<TJoinAgg, Guid>> secondLookup,
//            IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg> searcher)
//            : base(store)
//        {
//            this.firstLookup = firstLookup;
//            this.secondLookup = secondLookup;
//            this.ManyManyQueryFilter = searcher
//                                       ?? new ConventionalRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg>(
//                                                                                                                           this.firstLookup,
//                                                                                                                           this.secondLookup);
//        }
//
//        protected virtual void Parameters3(IWhereClauseBuilder<TSecondAgg> rs, TParameters parameters)
//        {
//            this.ManyManyQueryFilter.Where3(rs, parameters);
//        }
//
//        protected virtual void Parameters1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters)
//        {
//            this.ManyManyQueryFilter.Where1(rs, parameters);
//        }
//
//        protected virtual void Parameters2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters)
//        {
//            this.ManyManyQueryFilter.Where2(rs, parameters);
//        }
//
//        protected override Expression<Func<TJoinAgg, Guid>> Lookup(int typeIndex)
//        {
//            if (typeIndex == 0)
//            {
//                return this.firstLookup;
//            }
//
//            if (typeIndex == 1)
//            {
//                return this.secondLookup;
//            }
//
//            throw new ArgumentOutOfRangeException();
//        }
//
//        public virtual string LookupId(int index)
//        {
//            Expression<Func<TJoinAgg, Guid>> map1 = this.Lookup(index);
//
//            return (map1.Body as MemberExpression).Member.Name;
//        }
//
//        public async Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>>> FindPublishedItems(TParameters parameters)
//        {
//
//            var rootParameterExpression = this.RootParameterExpression();
//            var result1 = this.GetAggregate<TFirstAgg>(0, rootParameterExpression);
//            var result2 = this.GetAggregate<TSecondAgg>(1, rootParameterExpression);
//
//            var lambda1 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, bool>>(result1.Item1, rootParameterExpression, result1.Item2);
//
//            var lambda2 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, TSecondAgg, bool>>(
//                                                                                         result2.Item1,
//                                                                                         rootParameterExpression,
//                                                                                         result1.Item2,
//                                                                                         result2.Item2);
//
//            var items = await this.Store
//                                  .Select<TJoinAgg>()
//                                  .WithNoLock()
//                                  .Join<TFirstAgg>(lambda1)
//                                  .Join<TSecondAgg>(lambda2)
//                                  .Where1(rs => this.Parameters1(rs, parameters))
//                                  .Where2(rs => this.Parameters2(rs, parameters))
//                                  .Where3(rs => this.Parameters3(rs, parameters))
//                                  .Execute();
//            
//            return items.Select(x => new ObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>(x, y => y.Item1));
//        }
//    }
//
//    /*
//     
////            
////            var map1Property = this.FirstProperty();
////            var map2Property = this.SecondProperty();
////
////            var firstAggObjectId = typeof(TFirstAgg).GetInheritedProperty(nameof(Tables.es_agg_published.object_id));
////            var secondAggObjectId = typeof(TSecondAgg).GetInheritedProperty(nameof(Tables.es_agg_published.object_id));
////
////            ParameterExpression argParam2 = Expression.Parameter(typeof(TJoinAgg), "t2");
////            ParameterExpression argParam3 = Expression.Parameter(typeof(TFirstAgg), "t3");
////            ParameterExpression argParam4 = Expression.Parameter(typeof(TSecondAgg), "t4");
////
////            Expression nameProperty2_3 = map1Property.DeclaringType == typeof(TJoinAgg)
////                                             ? (Expression) Expression.Property(argParam2, map1Property)
////                                             : Expression.Property(Expression.Convert(argParam2, map1Property.DeclaringType), map1Property);
////
////            Expression nameProperty3 = Expression.Property(argParam3, firstAggObjectId);
////
////            Expression nameProperty2_4 = map2Property.DeclaringType == typeof(TJoinAgg)
////                                             ? (Expression) Expression.Property(argParam2, map2Property)
////                                             : Expression.Property(Expression.Convert(argParam2, map2Property.DeclaringType), map2Property);
////
////            Expression nameProperty4 = Expression.Property(argParam4, secondAggObjectId);
////
////            var eq1 = Expression.Equal(nameProperty2_3, nameProperty3);
////            var eq2 = Expression.Equal(nameProperty2_4, nameProperty4);
//     
//    public class GenericIsolatedSearchingManyManyDataAccessReader<TParameters, TJoinAgg, TFirstAgg, TSecondAgg>
//        : GenericIsolatedSearching<TParameters, TJoinAgg, TFirstAgg>,
//          IDataAccessSearcher<TParameters, Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>,
//          IRelatedDataAccessSearcher<TParameters, TJoinAgg, TFirstAgg, TSecondAgg>
//        where TJoinAgg : ITableRow, Tables.es_agg_published
//        where TFirstAgg : ITableRow, Tables.es_agg_published
//        where TSecondAgg : ITableRow, Tables.es_agg_published
//    {
//        private readonly Expression<Func<TJoinAgg, Guid>> secondLookup;
//
//        protected new IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg> ManyManyQueryFilter { get; }
//
//        public GenericIsolatedSearchingManyManyDataAccessReader(
//            IDataStore store,
//            Expression<Func<TJoinAgg, Guid>> firstLookup,
//            Expression<Func<TJoinAgg, Guid>> secondLookup,
//            IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg> searcher)
//            : base(store, firstLookup, searcher)
//        {
//            this.secondLookup = secondLookup;
//            this.ManyManyQueryFilter = searcher ?? new ConventionalRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg>(
//                                                                                                                                    this.firstLookup,
//                                                                                                                                    this
//                                                                                                                                        .secondLookup);
//            this.Store = store;
//        }
//
//        public async Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>>> FindPublishedItems(TParameters parameters)
//        {
//            return await this.SearchFor(parameters);
//        }
//
//        private async Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>>> SearchFor(TParameters parameters)
//        {
//            var map1Property = this.FirstProperty();
//            var map2Property = this.SecondProperty();
//
//            var firstAggObjectId = typeof(TFirstAgg).GetInheritedProperty(nameof(Tables.es_agg_published.object_id));
//            var secondAggObjectId = typeof(TSecondAgg).GetInheritedProperty(nameof(Tables.es_agg_published.object_id));
//
//            ParameterExpression argParam2 = Expression.Parameter(typeof(TJoinAgg), "t2");
//            ParameterExpression argParam3 = Expression.Parameter(typeof(TFirstAgg), "t3");
//            ParameterExpression argParam4 = Expression.Parameter(typeof(TSecondAgg), "t4");
//
//            Expression nameProperty2_3 = map1Property.DeclaringType == typeof(TJoinAgg)
//                                             ? (Expression) Expression.Property(argParam2, map1Property)
//                                             : Expression.Property(Expression.Convert(argParam2, map1Property.DeclaringType), map1Property);
//
//            Expression nameProperty3 = Expression.Property(argParam3, firstAggObjectId);
//
//            Expression nameProperty2_4 = map2Property.DeclaringType == typeof(TJoinAgg)
//                                             ? (Expression) Expression.Property(argParam2, map2Property)
//                                             : Expression.Property(Expression.Convert(argParam2, map2Property.DeclaringType), map2Property);
//
//            Expression nameProperty4 = Expression.Property(argParam4, secondAggObjectId);
//
//            var eq1 = Expression.Equal(nameProperty2_3, nameProperty3);
//            var eq2 = Expression.Equal(nameProperty2_4, nameProperty4);
//
//            var lambda1 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, bool>>(eq1, argParam2, argParam3);
//            var lambda2 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, TSecondAgg, bool>>(eq2, argParam2, argParam3, argParam4);
//
//            var items = await this.Store
//                                  .Select<TJoinAgg>()
//                                  .WithNoLock()
//                                  .Join<TFirstAgg>(lambda1)
//                                  .Join<TSecondAgg>(lambda2)
//                                  .Where1(rs => this.Parameters1(rs, parameters))
//                                  .Where2(rs => this.Parameters2(rs, parameters))
//                                  .Where3(rs => this.Parameters3(rs, parameters))
////                                  .Join<TObjectVersionTable>((x, y, z, a) => x.version_id == a.version_id)
//                                  .Execute();
//
//            return items.Select(x => new ObjectEntityRow(x));
//        }
//
//        protected virtual void Parameters3(IWhereClauseBuilder<TSecondAgg> rs, TParameters parameters)
//        {
//            this.ManyManyQueryFilter.Where3(rs, parameters);
//        }
//
//        protected virtual void Parameters1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters)
//        {
//            this.ManyManyQueryFilter.Where1(rs, parameters);
//        }
//
//        protected virtual void Parameters2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters)
//        {
//            this.ManyManyQueryFilter.Where2(rs, parameters);
//        }
//
//        protected virtual PropertyInfo SecondProperty()
//        {
//            var map1Property = typeof(TJoinAgg).GetInheritedProperty(this.SecondId());
//            return map1Property;
//        }
//
//        protected virtual PropertyInfo FirstProperty()
//        {
//            var map1Property = typeof(TJoinAgg).GetInheritedProperty(this.FirstId());
//            return map1Property;
//        }
//
//        public virtual string SecondId()
//        {
//            Expression<Func<TJoinAgg, Guid>> map1 = this.secondLookup;
//
//            return (map1.Body as MemberExpression).Member.Name;
//        }
//
//        public virtual string FirstId()
//        {
//            Expression<Func<TJoinAgg, Guid>> map1 = this.firstLookup;
//
//            return (map1.Body as MemberExpression).Member.Name;
//        }
//
//        public class ObjectEntityRow : IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>
//        {
//            private readonly Tuple<TJoinAgg, TFirstAgg, TSecondAgg> tuple;
//
//            public ObjectEntityRow(Tuple<TJoinAgg, TFirstAgg, TSecondAgg> tuple)
//            {
//                this.tuple = tuple;
//            }
//
//            public Guid Id => this.tuple.Item1.object_id;
//            public IVersionData RepresentedVersion => new VersionData(this.tuple.Item1);
//            public Tuple<TJoinAgg, TFirstAgg, TSecondAgg> Data => Tuple.Create(this.tuple.Item1, this.tuple.Item2, this.tuple.Item3);
//        }
//
//        private class VersionData : IVersionData
//        {
//            public VersionData(Tables.es_agg_published versionData)
//            {
//                this.VersionNumber = versionData.version_number.GetValueOrDefault();
//                this.VersionId = versionData.version_id.GetValueOrDefault();
//                this.CreatedFrom = versionData.created_from;
//                this.CreatedBy = versionData.created_by;
//                this.CreatedAt = versionData.created_at;
//            }
//
//            public int VersionNumber { get; }
//            public Guid VersionId { get; }
//            public Guid? CreatedFrom { get; }
//            public Guid CreatedBy { get; }
//            public DateTime CreatedAt { get; }
//        }
//    }*/
//}