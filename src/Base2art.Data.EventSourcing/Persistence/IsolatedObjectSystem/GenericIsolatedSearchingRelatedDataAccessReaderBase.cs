namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using DataStorage;
    using Modeling;

    public abstract class GenericIsolatedSearchingRelatedDataAccessReaderBase<TParameters, TJoinAgg, T>
        where TJoinAgg : ITableRow, Tables.es_agg_published
        where T : System.Collections.IStructuralComparable, System.Collections.IStructuralEquatable
    // WHERE T : Tuple<TJoinAgg, ...>
    {
        protected IDataStore Store { get; }

        protected GenericIsolatedSearchingRelatedDataAccessReaderBase(
            IDataStore store)
        {
            this.Store = store;
        }

        protected Tuple<BinaryExpression, ParameterExpression> GetAggregate<TAgg>(int i, ParameterExpression argParam2)
        {
            ParameterExpression argParam3 = this.GetParamExpression<TAgg>(i);
            var map1Property = this.Property(i);
            Expression nameProperty2_3 = GetNameProperty(map1Property, argParam2);

            var firstAggObjectId = typeof(TAgg).GetInheritedProperty(nameof(Tables.es_agg_published.object_id));
            Expression nameProperty3 = Expression.Property(argParam3, firstAggObjectId);

            var eq1 = Expression.Equal(nameProperty2_3, nameProperty3);
            return Tuple.Create(eq1, argParam3);
        }

        public virtual string LookupId(int index)
        {
            Expression<Func<TJoinAgg, Guid>> map1 = this.Lookup(index);

            return (map1.Body as MemberExpression).Member.Name;
        }

        private PropertyInfo PropertyOf(int typeIndex)
        {
            Expression<Func<TJoinAgg, Guid>> map1 = this.Lookup(typeIndex);

            var firstId = (map1.Body as MemberExpression).Member.Name;
            var map1Property = typeof(TJoinAgg).GetInheritedProperty(firstId);
            return map1Property;
        }

        protected abstract Expression<Func<TJoinAgg, Guid>> Lookup(int typeIndex);

        protected MethodInfo Property(int typeIndex)
        {
            return this.PropertyOf(typeIndex).GetMethod;
        }

        protected virtual ParameterExpression RootParameterExpression() => Expression.Parameter(typeof(TJoinAgg), "t1");

        private static Expression GetNameProperty(MethodInfo map1Property, ParameterExpression argParam2) =>
            map1Property.DeclaringType == typeof(TJoinAgg)
                ? (Expression) Expression.Property(argParam2, map1Property)
                : Expression.Property(Expression.Convert(argParam2, map1Property.DeclaringType), map1Property);

        protected virtual ParameterExpression GetParamExpression<TType>(int typeIndex)
        {
            var tupleType = typeof(T);   
            if (typeIndex <= tupleType.GetGenericArguments().Length)
            {
                return Expression.Parameter(typeof(TType), $"t{(typeIndex + 2)}");
            }

            throw new ArgumentOutOfRangeException(nameof(typeIndex));
        }

        protected class ObjectEntityRow<T> : IObjectEntityRow<T>
        {
            private readonly T tuple;
            private readonly Func<T, TJoinAgg> idLookup;

            public ObjectEntityRow(T tuple, Func<T, TJoinAgg> idLookup)
            {
                this.tuple = tuple;
                this.idLookup = idLookup;
            }

            public Guid Id => this.idLookup(this.tuple).object_id;
            public IVersionData RepresentedVersion => new VersionData(this.idLookup(this.tuple));
            public T Data => this.tuple;
        }

        private class VersionData : IVersionData
        {
            public VersionData(Tables.es_agg_published versionData)
            {
                this.VersionNumber = versionData.version_number.GetValueOrDefault();
                this.VersionId = versionData.version_id.GetValueOrDefault();
                this.CreatedFrom = versionData.created_from;
                this.CreatedBy = versionData.created_by;
                this.CreatedAt = versionData.created_at;
            }

            public int VersionNumber { get; }
            public Guid VersionId { get; }
            public Guid? CreatedFrom { get; }
            public Guid CreatedBy { get; }
            public DateTime CreatedAt { get; }
        }
    }
}