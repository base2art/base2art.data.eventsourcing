namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Threading.Tasks;

    public interface IRepairer
    {
        Task RepairPublished(Guid id);
    }
}