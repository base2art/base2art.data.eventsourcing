namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Threading.Tasks;
    using DataStorage;
    using Modeling;
    using Tables;

    public class GenericIsolatedDataAccess<T, TObjectTable, TObjectVersionTable, TPublicationTable>
        : IDataAccessReader<T>, IDataAccessWriter<T>
        where T : ITableRow
        where TObjectTable : es_object
        where TObjectVersionTable : es_version
        where TPublicationTable : class, es_version_publication
    {
        private readonly GenericIsolatedDataAccessReader<T, TObjectTable, TObjectVersionTable, TPublicationTable> reader;

        private readonly GenericIsolatedDataAccessWriter<T, TObjectTable, TObjectVersionTable, TPublicationTable> writer;

        public GenericIsolatedDataAccess(IDataBinder<T> dataBinder, IDataStore store, IDataAccessRepairQueue queue)
        {
            this.reader = new GenericIsolatedDataAccessReader<T, TObjectTable, TObjectVersionTable, TPublicationTable>(store);
            this.writer = new GenericIsolatedDataAccessWriter<T, TObjectTable, TObjectVersionTable, TPublicationTable>(dataBinder, store, queue);
        }

        public Task<ObjectVersionId> CreateObject(T data, Guid userId)
            => this.writer.CreateObject(data, userId);

        public Task<Guid> InsertVersion(Guid id, T data, Guid previousVersion, Guid userId)
            => this.writer.InsertVersion(id, data, previousVersion, userId);

        public Task UnpublishObject(Guid id, Guid userId)
            => this.writer.UnpublishObject(id, userId);

        public Task PublishVersion(Guid id, Guid version, Guid userId)
            => this.writer.PublishVersion(id, version, userId);

        public Task<IObjectEntity<T>> GetPublishedItem(Guid id)
            => this.reader.GetPublishedItem(id);

        public Task<IObjectEntity<T>> GetLatestItem(Guid id)
            => this.reader.GetLatestItem(id);

        public Task<IObjectEntity<T>> GetItem(Guid id, Guid versionId)
            => this.reader.GetItem(id, versionId);

        public async Task CreateTables(IDbms dbms)
        {
            await dbms.CreateTable<T>().IfNotExists().Execute();

            await dbms.CreateTable<TObjectTable>().IfNotExists().Execute();
            await dbms.CreateTable<TObjectVersionTable>().IfNotExists().Execute();
            await dbms.CreateTable<TPublicationTable>().IfNotExists().Execute();
        }

    }
}