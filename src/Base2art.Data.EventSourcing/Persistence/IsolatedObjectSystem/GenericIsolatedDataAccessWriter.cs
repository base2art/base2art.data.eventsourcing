namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using DataStorage;
    using Enumerable = System.Linq.Enumerable;

    public class GenericIsolatedDataAccessWriter<T, TObjectTable, TObjectVersionTable, TPublicationTable>
        : IDataAccessWriter<T>
        where T : ITableRow
        where TObjectTable : Tables.es_object
        where TObjectVersionTable : Tables.es_version
        where TPublicationTable : class, Tables.es_version_publication

    {
        private readonly IDataBinder<T> binder;
        private readonly IDataAccessRepairQueue queue;

        protected IDataStore Store { get; }

        public IDataBinder<T> Binder
        {
            get { return this.binder ?? new DynamicBinder<T>(); }
        }

        public GenericIsolatedDataAccessWriter(IDataBinder<T> binder, IDataStore store, IDataAccessRepairQueue queue)
        {
            this.binder = binder;
            this.queue = queue;
            this.Store = store;
        }

        public async Task<ObjectVersionId> CreateObject(T data, Guid userId)
        {
            var id = Guid.NewGuid();
            await this.Store.Insert<TObjectTable>()
                      .Record(rs => rs.Field(x => x.object_id, id))
                      .Execute();

            var versionId = await this.InsertVersionFrom(id, data, null, userId);
            await this.Repair(id);
            return new ObjectVersionId(id, versionId);
        }

        public async Task<Guid> InsertVersion(Guid id, T data, Guid previousVersion, Guid userId)
        {
            var x = await this.InsertVersionFrom(id, data, previousVersion, userId);
            await this.Repair(id);
            return x;
        }

        public async Task UnpublishObject(Guid id, Guid userId)
        {
            await this.PublishVersionInternal(id, null, userId);

            await this.Repair(id);
        }

        public async Task PublishVersion(Guid id, Guid version, Guid userId)
        {
            await this.PublishVersionInternal(id, version, userId);
            await this.Repair(id);
        }

        private async Task Repair(Guid id)
        {
            if (this.queue == null)
            {
                return;
            }

            await this.queue.PushRepairAsync(id, typeof(T));
        }

        private async Task PublishVersionInternal(Guid id, Guid? version, Guid userId)
        {
            var publication = await this.Store.GetLatestPublication<TPublicationTable>(id);

            Task RemoveItem()
            {
                var prev = publication?.id;
                return this.Store.Insert<TPublicationTable>()
                           .Record(rs => rs.Field(x => x.id, Guid.NewGuid())
                                           .Field(x => x.previously_publication_id, prev)
                                           .Field(x => x.version_id, version)
                                           .Field(x => x.object_id, id)
                                           .Field(x => x.created_by, userId)
                                           .Field(x => x.version_created_from, null)
                                           .Field(x => x.version_number, null)
                                           .Field(x => x.created_at, DateTime.Now))
                           .Execute();
            }

            if (version == null)
            {
                await RemoveItem();
                return;
            }

            var versionData = await this.Store.SelectSingle<TObjectTable>()
                                        .Join<TObjectVersionTable>((x, y) => x.object_id == y.object_id)
                                        .Join<T>((x, y, z) => y.version_id == z.id)
                                        .Where1(rs => rs.Field(x => x.object_id, id, (g1, g2) => g1 == g2))
                                        .Where2(rs => rs.Field(x => x.version_id, version.Value, (g1, g2) => g1 == g2))
                                        .Execute();

            if (versionData == null)
            {
                await RemoveItem();
                return;
            }

            Console.WriteLine(id);

            var prev1 = publication?.id;
            await this.Store.Insert<TPublicationTable>()
                      .Record(rs => rs.Field(x => x.id, Guid.NewGuid())
                                      .Field(x => x.previously_publication_id, prev1)
                                      .Field(x => x.version_id, version)
                                      .Field(x => x.object_id, id)
                                      .Field(x => x.created_by, userId)
                                      .Field(x => x.version_created_from, versionData.Item2.created_from)
                                      .Field(x => x.created_at, versionData.Item2.created_at))
                      .Execute();
        }

        private async Task<Guid> InsertVersionFrom(Guid id, T data, Guid? previousVersion, Guid userId)
        {
            var versionId = Guid.NewGuid();

            var newVersionNumber = 0;
            if (previousVersion.HasValue)
            {
                var previous = await this.Store.SelectSingle<TObjectVersionTable>()
                                         .Where(rs => rs.Field(x => x.version_id, previousVersion.Value, (x, y) => x == y))
                                         .Execute();

                if (previous == null)
                {
                    throw new InvalidOperationException("previousRecordNotFound");
                }

                newVersionNumber = previous.version_number + 1;
            }

            await this.Store.Insert<TObjectVersionTable>()
                      .Record(rs => rs.Field(x => x.created_at, DateTime.UtcNow)
                                      .Field(x => x.created_by, userId)
                                      .Field(x => x.created_from, previousVersion)
                                      .Field(x => x.object_id, id)
                                      .Field(x => x.version_id, versionId)
                                      .Field(x => x.version_number, newVersionNumber))
                      .Execute();

            await this.Store.Insert<T>()
                      .Record(rs => this.Binder.Bind(rs.Field(x => x.id, versionId), data))
                      .Execute();

            return versionId;
        }
    }
}