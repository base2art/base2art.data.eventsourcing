namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataStorage;
    using DataStorage.DataManipulation;
    using Modeling;

    public class GenericIsolatedDataAccessReader<T, TObjectTable, TObjectVersionTable, TPublicationTable> : IDataAccessReader<T>
        where T : ITableRow
        where TObjectTable : Tables.es_object
        where TObjectVersionTable : Tables.es_version
        where TPublicationTable : class, Tables.es_version_publication
    {
        protected IDataStore Store { get; }

        public GenericIsolatedDataAccessReader(IDataStore store)
        {
            this.Store = store;
        }

        public async Task<IObjectEntity<T>> GetPublishedItem(Guid id)
        {
            var first = await this.Store.GetLatestPublication<TPublicationTable>(id);
            if (first?.version_id == null)
            {
                return null;
            }

            return await this.GetByVersion(id, first.version_id.Value, null);
        }

        public async Task<IObjectEntity<T>> GetLatestItem(Guid id)
        {
            var versions = (await this.GetVersions(id)).ToArray();
            var max = versions.Max(x => x.version_number);
            var currentVersion = versions.First(x => x.version_number == max);

            return await this.GetByVersion(id, currentVersion.version_id, versions);
        }

        private async Task<IObjectEntity<T>> GetByVersion(Guid id, Guid currentVersionId, TObjectVersionTable[] allVersions)
        {
            if (allVersions == null)
            {
                allVersions = (await this.GetVersions(id)).ToArray();
            }

            var objects = await this.QueryOfObject(id)
                                    .Where2(rs => rs.Field(x => x.version_id, currentVersionId, (a, b) => a == b))
                                    .Execute();

            var first = objects.FirstOrDefault();
            if (first == null)
            {
                return null;
            }

            return new ObjectEntity<T, TObjectTable, TObjectVersionTable, TPublicationTable>(first.Item1, first.Item3, allVersions, first.Item2);
        }

        public Task<IObjectEntity<T>> GetItem(Guid id, Guid versionId)
        {
            return this.GetByVersion(id, versionId, null);
        }

        private IQuerySelect<TObjectTable, TObjectVersionTable, T> QueryOfObject(Guid id)
        {
            //this.binder.GetMatchExpression()
            return this.Store.Select<TObjectTable>()
                       .Join<TObjectVersionTable>((x, y) => x.object_id == y.object_id)
                       .Join<T>((x, y, z) => y.version_id == z.id)
                       .Where1(rs => rs.Field(x => x.object_id, id, (g1, g2) => g1 == g2));
        }

        private async Task<IEnumerable<TObjectVersionTable>> GetVersions(Guid id)
        {
            var versions = await this.Store.Select<TObjectVersionTable>()
                                     .Where(rs => rs.Field(x => x.object_id, id, (g1, g2) => g1 == g2))
                                     .Execute();
            return versions;
        }
    }
}