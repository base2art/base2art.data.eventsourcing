namespace Base2art.Data.EventSourcing.Persistence.IsolatedObjectSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Threading.Tasks;
    using DataStorage;
    using DataStorage.DataManipulation;
    using DataStorage.DataManipulation.Builders;
    using Modeling;

    public class GenericIsolatedSearchingRelatedDataAccessReader<TParameters, TJoinAgg, TFirstAgg, TSecondAgg>
        : GenericIsolatedSearchingRelatedDataAccessReaderBase<TParameters, TJoinAgg, Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>,
          IDataAccessSearcher<TParameters, Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>,
          IRelatedDataAccessSearcher<TParameters, TJoinAgg, TFirstAgg, TSecondAgg>
        where TJoinAgg : ITableRow, Tables.es_agg_published
        where TFirstAgg : ITableRow, Tables.es_agg_published
        where TSecondAgg : ITableRow, Tables.es_agg_published
    {
        private readonly Expression<Func<TJoinAgg, Guid>> firstLookup;
        private readonly Expression<Func<TJoinAgg, Guid>> secondLookup;

        protected IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg> ManyManyQueryFilter { get; }

        public GenericIsolatedSearchingRelatedDataAccessReader(
            IDataStore store,
            Expression<Func<TJoinAgg, Guid>> firstLookup,
            Expression<Func<TJoinAgg, Guid>> secondLookup,
            IRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg> searcher)
            : base(store)
        {
            this.firstLookup = firstLookup;
            this.secondLookup = secondLookup;
            this.ManyManyQueryFilter = searcher
                                       ?? new ConventionalRelatedQueryFilter<TParameters, TJoinAgg, TFirstAgg, TSecondAgg>(
                                                                                                                           this.firstLookup,
                                                                                                                           this.secondLookup);
        }

        protected virtual void Parameters3(IWhereClauseBuilder<TSecondAgg> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where3(rs, parameters);
        }

        protected virtual void Parameters1(IWhereClauseBuilder<TJoinAgg> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where1(rs, parameters);
        }

        protected virtual void Parameters2(IWhereClauseBuilder<TFirstAgg> rs, TParameters parameters)
        {
            this.ManyManyQueryFilter.Where2(rs, parameters);
        }

        protected override Expression<Func<TJoinAgg, Guid>> Lookup(int typeIndex)
        {
            if (typeIndex == 0)
            {
                return this.firstLookup;
            }

            if (typeIndex == 1)
            {
                return this.secondLookup;
            }

            throw new ArgumentOutOfRangeException();
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>>> FindPublishedItems(TParameters parameters)
        {
            return this.SearchFor(parameters, x => x);
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>>> FindPublishedItems(
            TParameters parameters,
            IndexPagination pagination)
        {
            return this.SearchFor(parameters, x => x.Limit(pagination.PageSize).Offset(pagination.PageIndex * pagination.PageSize));
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>>> FindPublishedItems(
            TParameters parameters,
            OffsetPagination pagination)
        {
            return this.SearchFor(parameters, x => x);
        }

        private async Task<IEnumerable<IObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>>> SearchFor(
            TParameters parameters,
            Func<IQuerySelect<TJoinAgg>, IQuerySelect<TJoinAgg>> lookup)
        {
            var rootParameterExpression = this.RootParameterExpression();
            var result1 = this.GetAggregate<TFirstAgg>(0, rootParameterExpression);
            var result2 = this.GetAggregate<TSecondAgg>(1, rootParameterExpression);

            var lambda1 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, bool>>(result1.Item1, rootParameterExpression, result1.Item2);

            var lambda2 = Expression.Lambda<Func<TJoinAgg, TFirstAgg, TSecondAgg, bool>>(
                                                                                         result2.Item1,
                                                                                         rootParameterExpression,
                                                                                         result1.Item2,
                                                                                         result2.Item2);

            var items = await lookup(this.Store.Select<TJoinAgg>())
                              .WithNoLock()
                              .Join<TFirstAgg>(lambda1)
                              .Join<TSecondAgg>(lambda2)
                              .Where1(rs => this.Parameters1(rs, parameters))
                              .Where2(rs => this.Parameters2(rs, parameters))
                              .Where3(rs => this.Parameters3(rs, parameters))
                              .Execute();

            return items.Select(x => new ObjectEntityRow<Tuple<TJoinAgg, TFirstAgg, TSecondAgg>>(x, y => y.Item1));
        }
    }
}