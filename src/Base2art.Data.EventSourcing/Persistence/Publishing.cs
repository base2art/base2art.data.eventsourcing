namespace Base2art.Data.EventSourcing.Persistence
{
    using System;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using DataStorage;
    using Tables;

    public static class Publishing
    {
        public static async Task<T> GetLatestPublication<T>(this IDataStore store, Guid objectId)
            where T : class, es_version_publication
        {
            var objects = await store.Select<T>()
                                     .LeftJoin<T>((x, y) => x.id == y.previously_publication_id)
                                     .Where1(rs => rs.Field(x => x.object_id, objectId, (a, b) => a == b))
                                     .Where2(rs => rs.Field(x => x.object_id, null, (a, b) => a == b))
                                     .Execute();

            var first = objects.FirstOrDefault();
            return first?.Item1;
        }

        public static async Task<Tuple<TPublish, TVersion>> GetLatestPublicationWithVersion<TPublish, TVersion>(this IDataStore store, Guid objectId)
            where TPublish : class, es_version_publication
            where TVersion : class, es_version
        {
            var objects = await store.Select<TPublish>()
                                     .LeftJoin<TPublish>((x, y) => x.id == y.previously_publication_id)
                                     .Join<TVersion>((x, y, z) => x.version_id == z.version_id)
                                     .Where1(rs => rs.Field(x => x.object_id, objectId, (a, b) => a == b))
                                     .Where2(rs => rs.Field(x => x.object_id, null, (a, b) => a == b))
                                     .Execute();

            var first = objects.FirstOrDefault();
            var esVersionPublication = first?.Item1;
            if (esVersionPublication == null)
            {
                return null;
            }

            return Tuple.Create(esVersionPublication, first.Item3);
        }
    }
}