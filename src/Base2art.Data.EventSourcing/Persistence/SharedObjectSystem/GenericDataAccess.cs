namespace Base2art.Data.EventSourcing.Persistence.SharedObjectSystem
{
    using System;
    using System.Threading.Tasks;
    using DataStorage;
    using IsolatedObjectSystem;
    using Modeling;
    using Tables;

    public class GenericDataAccess<T> : IDataAccessReader<T>, IDataAccessWriter<T>
        where T : ITableRow
    {
        private readonly GenericIsolatedDataAccess<T, es_shared_object, es_shared_version, es_shared_version_publication> backing;

        public GenericDataAccess(IDataBinder<T> dataBinder, IDataStore store, IDataAccessRepairQueue queue)
        {
            this.backing = new GenericIsolatedDataAccess<T, es_shared_object, es_shared_version, es_shared_version_publication>(
                                                                                                                                dataBinder,
                                                                                                                                store,
                                                                                                                                queue);
        }

        public Task<ObjectVersionId> CreateObject(T data, Guid userId)
            => this.backing.CreateObject(data, userId);

        public Task<Guid> InsertVersion(Guid id, T data, Guid previousVersion, Guid userId)
            => this.backing.InsertVersion(id, data, previousVersion, userId);

        public Task UnpublishObject(Guid id, Guid userId)
            => this.backing.UnpublishObject(id, userId);

        public Task PublishVersion(Guid id, Guid version, Guid userId)
            => this.backing.PublishVersion(id, version, userId);

        public Task<IObjectEntity<T>> GetPublishedItem(Guid id)
            => this.backing.GetPublishedItem(id);

        public Task<IObjectEntity<T>> GetLatestItem(Guid id)
            => this.backing.GetLatestItem(id);

        public Task<IObjectEntity<T>> GetItem(Guid id, Guid versionId)
            => this.backing.GetItem(id, versionId);

        public Task CreateTables(IDbms dbms)
            => this.backing.CreateTables(dbms);

//        public void Register(Action<T, es_shared_object, es_shared_version, es_shared_version_publication> process)
//            => this.backing.Register(process);
    }
}