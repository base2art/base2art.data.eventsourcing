namespace Base2art.Data.EventSourcing.Persistence.SharedObjectSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using DataStorage;
    using Modeling;
    using Tables;
    using IsolatedObjectSystem;

    public class GenericSearchingDataAccess<TParameters, TJoin> : IDataAccessSearcher<TParameters, TJoin>
        where TJoin : ITableRow, es_agg_published
    {
        private readonly GenericIsolatedSearchingDataAccessReader<TParameters, TJoin> backing;

        public GenericSearchingDataAccess(
            IDataStore store,
            IQueryFilter<TParameters, TJoin> searcher)
        {
            this.backing = new GenericIsolatedSearchingDataAccessReader<TParameters, TJoin>(store, searcher);
        }

        public Task<IEnumerable<IObjectEntityRow<TJoin>>> FindPublishedItems(TParameters parameters)
        {
            return this.backing.FindPublishedItems(parameters);
        }

        public Task<IEnumerable<IObjectEntityRow<TJoin>>> FindPublishedItems(TParameters parameters, IndexPagination pagination)
        {
            return this.backing.FindPublishedItems(parameters, pagination);
        }

        public Task<IEnumerable<IObjectEntityRow<TJoin>>> FindPublishedItems(TParameters parameters, OffsetPagination pagination)
        {
            return this.backing.FindPublishedItems(parameters, pagination);
        }
    }
}