namespace Base2art.Data.EventSourcing.Persistence.SharedObjectSystem
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using DataStorage;
    using IsolatedObjectSystem;
    using Modeling;
    using Tables;

    public class GenericSearchingManyManyDataAccess<TParameters, TJoin, TFirst, TSecond>
        : IDataAccessSearcher<TParameters, Tuple<TJoin, TFirst, TSecond>>,
          IRelatedDataAccessSearcher<TParameters, TJoin, TFirst, TSecond>
        where TJoin : ITableRow, es_agg_published
        where TFirst : ITableRow, es_agg_published
        where TSecond : ITableRow, es_agg_published
    {
        private readonly GenericIsolatedSearchingRelatedDataAccessReader<TParameters, TJoin, TFirst, TSecond> backing;

        public GenericSearchingManyManyDataAccess(
            IDataStore store,
            Expression<Func<TJoin, Guid>> firstLookup,
            Expression<Func<TJoin, Guid>> secondLookup,
            IRelatedQueryFilter<TParameters, TJoin, TFirst, TSecond> searcher)
        {
            this.backing = new GenericIsolatedSearchingRelatedDataAccessReader<TParameters, TJoin, TFirst, TSecond>(
                                                                                                                    store,
                                                                                                                    firstLookup,
                                                                                                                    secondLookup,
                                                                                                                    searcher);
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoin, TFirst, TSecond>>>> FindPublishedItems(TParameters parameters)
        {
            return this.backing.FindPublishedItems(parameters);
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoin, TFirst, TSecond>>>> FindPublishedItems(
            TParameters parameters, IndexPagination pagination)
        {
            return this.backing.FindPublishedItems(parameters, pagination);
        }

        public Task<IEnumerable<IObjectEntityRow<Tuple<TJoin, TFirst, TSecond>>>> FindPublishedItems(
            TParameters parameters, OffsetPagination pagination)
        {
            return this.backing.FindPublishedItems(parameters, pagination);
        }

        public virtual string SecondId()
            => this.backing.LookupId(1);

        public virtual string FirstId()
            => this.backing.LookupId(0);
    }
}