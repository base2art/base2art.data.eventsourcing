namespace Base2art.Data.EventSourcing
{
    using System;
    using System.Threading.Tasks;
    using Modeling;

    public interface IDataAccessReader<TObjectEntityData>
    {
        Task<IObjectEntity<TObjectEntityData>> GetPublishedItem(Guid id);

        Task<IObjectEntity<TObjectEntityData>> GetLatestItem(Guid id);

        Task<IObjectEntity<TObjectEntityData>> GetItem(Guid id, Guid versionId);
    }
}