namespace Base2art.Data.EventSourcing
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Modeling;

    public interface IDataAccessSearcher<in TParameters, TObjectEntityData>
    {
        Task<IEnumerable<IObjectEntityRow<TObjectEntityData>>> FindPublishedItems(TParameters parameters);

        Task<IEnumerable<IObjectEntityRow<TObjectEntityData>>> FindPublishedItems(TParameters parameters, IndexPagination pagination);

//        Task<IEnumerable<IObjectEntityRow<TObjectEntityData>>> FindPublishedItems(TParameters parameters, OffsetPagination pagination);
    }
}