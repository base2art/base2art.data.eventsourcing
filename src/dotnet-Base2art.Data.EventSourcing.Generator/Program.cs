﻿namespace Base2art.Data.EventSourcing.Generator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    using CodeGen;
    using CommandLine;

    class Program
    {
        public static async Task<int> Main(string[] args)
        {
            IReadOnlyList<string> compile = Array.Empty<string>();
            IReadOnlyList<string> refs = Array.Empty<string>();
            IReadOnlyList<string> preprocessorSymbols = Array.Empty<string>();
            IReadOnlyList<string> generatorSearchPaths = Array.Empty<string>();
            string generatedCompileItemFile = null;
            string outputDirectory = null;
            string projectDir = null;
            bool version = false;
            ArgumentSyntax.Parse(args, syntax =>
            {
                syntax.DefineOption("version", ref version, false, "Show version of this tool (and exits).");
                syntax.DefineOptionList("r|reference", ref refs, false, "Paths to assemblies being referenced");
                syntax.DefineOptionList("d|define", ref preprocessorSymbols, false, "Preprocessor symbols");
                syntax.DefineOptionList("generatorSearchPath", ref generatorSearchPaths, false,
                                        "Paths to folders that may contain generator assemblies");
                syntax.DefineOption("out", ref outputDirectory, false, "The directory to write generated source files to");
                syntax.DefineOption("projectDir", ref projectDir, true, "The absolute path of the directory where the project file is located");
                syntax.DefineOption("generatedFilesList", ref generatedCompileItemFile, false,
                                    "The path to the file to create with a list of generated source files");
                syntax.DefineParameterList("compile", ref compile, "Source files included in compilation");
            });

            AppDomain.CurrentDomain.AssemblyResolve += (sender, args1) =>
            {
                var objExecutingAssemblies = Assembly.GetExecutingAssembly();
                AssemblyName[] arrReferencedAssmbNames = objExecutingAssemblies.GetReferencedAssemblies();

                var requestedAssembly = args1.Name.Split(",")[0].Trim();

                if (string.Equals(requestedAssembly, "System.CodeDom", StringComparison.OrdinalIgnoreCase))
                {
                    var userProfile = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                    var systemCodeDom = Path.Combine(
                                                     userProfile,
                                                     ".nuget",
                                                     "packages",
                                                     "system.codedom",
                                                     "4.5.0",
                                                     "lib",
                                                     "netstandard2.0",
                                                     "System.CodeDom.dll");

                    var myAssembly = Assembly.LoadFile(systemCodeDom);

                    //Return the loaded assembly.
                    return myAssembly;
                }

                //Load the assembly from the specified path.
                return null;
            };

            var path = new DirectoryInfo(projectDir);
            var scanDir = Path.Combine(path.FullName, "Tables");

            if (string.IsNullOrWhiteSpace(outputDirectory))
            {
                outputDirectory = Path.Combine(path.FullName, "gen");
            }

            var gen = new CodeGen(Directory.CreateDirectory(scanDir), Directory.CreateDirectory(outputDirectory));
            await gen.Process(false);

            return 0;
        }
    }
}