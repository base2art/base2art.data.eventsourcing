namespace Base2art.Data.EventSourcing.MessageQueue
{
    using System;
    using System.Threading.Tasks;
    using Base2art.MessageQueue;
    using Base2art.MessageQueue.Management;
    using Persistence.IsolatedObjectSystem;

    public class MessageQueueRepair : IDataAccessRepairQueue
    {
        private readonly RepairHandlerContainer container;
        private readonly IMessageQueue queue;

        public MessageQueueRepair(IMessageQueueStore store, RepairHandlerContainer container)
        {
            this.container = container;
            this.queue = new SimpleMessageQueue(store);
        }

        public Task PushRepairAsync(Guid id, Type dataType)
        {
            return this.queue.Push(new RepairMessage {Id = id, DataType = dataType});
        }

        public void AddRepairer<T>(IRepairer repairer)
        {
            this.container.Handlers[typeof(T)] = repairer;
        }
    }
}