namespace Base2art.Data.EventSourcing.MessageQueue
{
    using System.Threading.Tasks;
    using Base2art.MessageQueue;

    public class RepairHandler : MessageHandlerBase<RepairMessage>
    {
        private readonly RepairHandlerContainer repairHandlerContainer;

        public RepairHandler(RepairHandlerContainer repairHandlerContainer)
        {
            this.repairHandlerContainer = repairHandlerContainer;
        }

        protected override async Task HandleMessage(RepairMessage message)
        {
            var repairer = this.repairHandlerContainer.Handlers[message.DataType];
            await repairer.RepairPublished(message.Id);
        }
    }
}