namespace Base2art.Data.EventSourcing.MessageQueue
{
    using System;
    using System.Collections.Generic;
    using Persistence.IsolatedObjectSystem;

    public class RepairHandlerContainer
    {
        private readonly Dictionary<Type, IRepairer> handlers = new Dictionary<Type, IRepairer>();

        public Dictionary<Type, IRepairer> Handlers
        {
            get { return this.handlers; }
        }
    }
}