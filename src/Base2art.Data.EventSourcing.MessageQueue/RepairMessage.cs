namespace Base2art.Data.EventSourcing.MessageQueue
{
    using System;
    using Base2art.MessageQueue;

    public class RepairMessage : MessageBase
    {
        public Type DataType { get; set; }
        public Guid Id { get; set; }
    }
}